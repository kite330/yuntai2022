package com.atguigu.yuntai.govern.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 治理项
 * </p>
 */
@Data
@ApiModel(description = "治理项")
@TableName("governance_option")
public class GovernanceOption implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 治理项编码
     */
    @ApiModelProperty(value = "治理项编码")
    @TableField("option_code")
    private String optionCode;

    /**
     * 治理项描述
     */
    @ApiModelProperty(value = "治理项描述")
    @TableField("option_desc")
    private String optionDesc;

    /**
     * 治理项类型
     */
    @ApiModelProperty(value = "治理项类型")
    @TableField("option_type")
    private String optionType;

    /**
     * 治理项分数
     */
    @ApiModelProperty(value = "治理项分数")
    @TableField("option_score")
    private BigDecimal optionScore;

}
