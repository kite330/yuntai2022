package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 生命周期完备评分器
 */
@Component("LIFE_CYCLE")
@Slf4j
public class TableLifecycleJudger implements  Judger{

    //生命周期
    public   final String OPTION_CODE="LIFE_CYCLE";
    //存储
    public   final String OPTION_TYPE="STORAGE";

    /**
     * 根据生命周期是否完整进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo,String busiDate) {
        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);


        if (tableInfo.getLifecycleDays()!=null&&tableInfo.getLifecycleDays()>0){
            governanceDetail.setGovernanceContent("符合规范");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }else {
            governanceDetail.setGovernanceContent("缺少生命周期配置");
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }


        return governanceDetail;
    }
}
