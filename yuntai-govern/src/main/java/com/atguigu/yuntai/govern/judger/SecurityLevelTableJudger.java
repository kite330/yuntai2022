package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import com.atguigu.yuntai.schedule.service.TDsTaskDefinitionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 是否有访问级别评分器
 */
@Component("HAS_SECURITY_LEVEL_TABLE")
@Slf4j
public class SecurityLevelTableJudger implements  Judger{

    //是否有访问级别选项code
    public   final String OPTION_CODE="HAS_SECURITY_LEVEL_TABLE";
    //监控类型-安全
    public   final String OPTION_TYPE="SECURITY";

    @Autowired
    TDsTaskDefinitionService tDsTaskDefinitionService;

    /**
     * 根据是否设置安全级别进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo,String busiDate) {
        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);
        governanceDetail.setOptionCode(OPTION_CODE);
        governanceDetail.setOptionType(OPTION_TYPE);

        if (tableInfo.getSecurityLevel()!=null &&tableInfo.getSecurityLevel().length()>0 ){
            governanceDetail.setGovernanceContent("已设置安全级别");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);

        }else {
            governanceDetail.setGovernanceContent("未设置安全级别");
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
        }
        return governanceDetail;
    }
}
