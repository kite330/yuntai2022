package com.atguigu.yuntai.govern.service;

import com.atguigu.yuntai.govern.entity.GovernanceUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 技术负责人治理明细 服务类
 * </p>
 */
public interface GovernanceUserService extends IService<GovernanceUser> {

}
