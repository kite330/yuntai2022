package com.atguigu.yuntai.govern.point;

import com.atguigu.yuntai.govern.entity.TableInfo;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;

/**
 * 切面类
 */
@Aspect
@Component
@Slf4j
public class Point {

    @Before("execution(* com.atguigu.yuntai.govern.judger.Judger.judge(..))")
    public void Before( JoinPoint point) {
        Object[] args = point.getArgs();
        TableInfo tableInfo =(TableInfo) args[0];
        String tableName = tableInfo.getTableName();

        Class<?> judgerClass = point.getTarget().getClass();
        Annotation[] annotations = judgerClass.getAnnotations();
        for (Annotation annotation : annotations) {
           if(annotation.annotationType()==Component.class ){
               Component component = judgerClass.getAnnotation(Component.class);
               log.info("----表名："+tableName+"---judger:"+component.value());
           }
        }

    }
}
