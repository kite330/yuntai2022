package com.atguigu.yuntai.govern.service.impl;

import com.atguigu.yuntai.govern.entity.GovernanceGlobal;
import com.atguigu.yuntai.govern.mapper.GovernanceGlobalMapper;
import com.atguigu.yuntai.govern.service.GovernanceGlobalService;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 治理全局情况 服务实现类
 * </p>
 */
@Service
public class GovernanceGlobalServiceImpl extends ServiceImpl<GovernanceGlobalMapper, GovernanceGlobal> implements GovernanceGlobalService {

}
