package com.atguigu.yuntai.govern.service.impl;

import com.atguigu.yuntai.govern.entity.GovernanceType;
import com.atguigu.yuntai.govern.mapper.GovernanceTypeMapper;
import com.atguigu.yuntai.govern.service.GovernanceTypeService;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 治理项类别 服务实现类
 * </p>
 */
@Service
public class GovernanceTypeServiceImpl extends ServiceImpl<GovernanceTypeMapper, GovernanceType> implements GovernanceTypeService {

    @Override
    public Map<String, GovernanceType> getGovernanceTypeMap() {

        Map<String, GovernanceType> governanceTypeMap=new HashMap<>();

        List<GovernanceType> governanceOptionList
                = list();


        for (GovernanceType governanceType : governanceOptionList) {
            governanceTypeMap.put(governanceType.getTypeCode(),governanceType);
        }

        return  governanceTypeMap;

    }
}
