package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import com.atguigu.yuntai.schedule.service.TDsTaskInstanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 是否运行报错评分器
 */
@Component("IS_RUN_ERROR")
@Slf4j
public class TableRunErrorJudger implements  Judger{

    //是否运行报错
    public   final String OPTION_CODE="IS_RUN_ERROR";
    //计算
    public   final String OPTION_TYPE="CALC";

    @Autowired
    TDsTaskInstanceService tDsTaskInstanceService;

    /**
     * 根据运行是否报错进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo,String busiDate) {
        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);

        Long countErrorTask = tDsTaskInstanceService.countErrorTaskByTableName(busiDate, tableInfo.getTableName());


        if (countErrorTask==0 ){
            governanceDetail.setGovernanceContent("不存在运行失败任务");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }else {
            governanceDetail.setGovernanceContent("存在"+countErrorTask+"个运行失败任务!");
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }


        return governanceDetail;
    }
}
