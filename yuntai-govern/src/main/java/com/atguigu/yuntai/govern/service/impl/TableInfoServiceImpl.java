package com.atguigu.yuntai.govern.service.impl;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.mapper.TableInfoMapper;
import com.atguigu.yuntai.govern.service.TableInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * <p>
 * 表元数据 服务实现类
 * </p>
 */
@Service
public class TableInfoServiceImpl extends ServiceImpl<TableInfoMapper, TableInfo> implements TableInfoService {

    @Autowired
    TableInfoService tableInfoService;
    @Autowired
    TableInfoMapper tableInfoMapper;

    Map<String, TableInfo> tableInfoMap = new HashMap<>();

    Long DEFAULT_TEC_OWNER = 0L;

    @PostConstruct
    public void initMap() {
        List<TableInfo> list = tableInfoService.list();
        for (TableInfo tableInfo : list) {
            tableInfoMap.put(tableInfo.getSchemaName() + "." + tableInfo.getTableName(), tableInfo);
        }

    }

    public String getGlobalTableSize() {
        Long globalTableSize = this.baseMapper.getGlobalTableSize();
        if (globalTableSize < (1024 * 1024)) {
            BigDecimal sizeK = BigDecimal.valueOf(globalTableSize).divide(BigDecimal.valueOf(1024), 2, RoundingMode.HALF_UP);
            return sizeK + " KB";
        } else if (globalTableSize < (1024 * 1024 * 1024)) {
            BigDecimal sizeM = BigDecimal.valueOf(globalTableSize).divide(BigDecimal.valueOf(1024 * 1024), 2, RoundingMode.HALF_UP);
            return sizeM + " MB";
        } else {
            BigDecimal sizeG = BigDecimal.valueOf(globalTableSize).divide(BigDecimal.valueOf(1024 * 1024 * 1024), 2, RoundingMode.HALF_UP);
            return sizeG + " GB";
        }
    }

}
