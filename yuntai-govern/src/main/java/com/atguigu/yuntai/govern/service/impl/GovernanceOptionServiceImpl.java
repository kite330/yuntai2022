package com.atguigu.yuntai.govern.service.impl;

import com.atguigu.yuntai.govern.entity.GovernanceOption;
import com.atguigu.yuntai.govern.mapper.GovernanceOptionMapper;
import com.atguigu.yuntai.govern.service.GovernanceOptionService;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 治理项 服务实现类
 * </p>
 */
@Service
public class GovernanceOptionServiceImpl extends ServiceImpl<GovernanceOptionMapper, GovernanceOption> implements GovernanceOptionService {

    public Map<String, GovernanceOption> getGovernanceOptionMap() {

        Map<String, GovernanceOption> governanceOptionMap = new HashMap<>();

        List<GovernanceOption> governanceOptionList = list(new QueryWrapper<GovernanceOption>().ne("option_score", "0"));

        for (GovernanceOption governanceOption : governanceOptionList) {
            governanceOptionMap.put(governanceOption.getOptionCode(), governanceOption);
        }

        return governanceOptionMap;

    }

    public Map<String, List<GovernanceOption>> getGovernanceOptionGroupByType() {
        List<GovernanceOption> governanceOptionList = list(new QueryWrapper<GovernanceOption>().ne("option_score", "0"));
        Map<String, List<GovernanceOption>> governanceOptionTypeMap = governanceOptionList.stream().collect(Collectors.groupingBy(GovernanceOption::getOptionType));
        return governanceOptionTypeMap;
    }

}
