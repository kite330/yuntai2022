package com.atguigu.yuntai.govern.service;

import com.atguigu.yuntai.govern.entity.GovernanceTable;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 表治理明细 服务类
 * </p>
 */
public interface GovernanceTableService extends IService<GovernanceTable> {

}
