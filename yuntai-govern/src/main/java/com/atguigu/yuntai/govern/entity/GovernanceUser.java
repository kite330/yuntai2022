package com.atguigu.yuntai.govern.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 技术负责人治理明细
 * </p>
 */
@Data
@ApiModel(description = "技术负责人治理明细")
@TableName("governance_user")
public class GovernanceUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 业务日期
     */
    @ApiModelProperty(value = "业务日期")
    @TableField("busi_date")
    private String busiDate;

    /**
     * 技术负责人
     */
    @ApiModelProperty(value = "技术负责人")
    @TableField("charge_tec_user_id")
    private Long chargeTecUserId;

    /**
     * 规范分数
     */
    @ApiModelProperty(value = "规范分数")
    @TableField("score_spec")
    private BigDecimal scoreSpec=BigDecimal.ZERO;

    /**
     * 存储分数
     */
    @ApiModelProperty(value = "存储分数")
    @TableField("score_storage")
    private BigDecimal scoreStorage=BigDecimal.ZERO;

    /**
     * 计算分数
     */
    @ApiModelProperty(value = "计算分数")
    @TableField("score_calc")
    private BigDecimal scoreCalc=BigDecimal.ZERO;

    /**
     * 质量分数
     */
    @ApiModelProperty(value = "质量分数")
    @TableField("score_quality")
    private BigDecimal scoreQuality=BigDecimal.ZERO;

    /**
     * 安全分数
     */
    @ApiModelProperty(value = "安全分数")
    @TableField("score_security")
    private BigDecimal scoreSecurity=BigDecimal.ZERO;

    /**
     * 分数
     */
    @ApiModelProperty(value = "分数")
    @TableField("score")
    private BigDecimal score=BigDecimal.ZERO;

    /**
     * 涉及表
     */
    @ApiModelProperty(value = "涉及表")
    @TableField("table_num")
    private Long tableNum=0L;

    /**
     * 问题项个数
     */
    @ApiModelProperty(value = "问题项个数")
    @TableField("problem_num")
    private Long problemNum=0L;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * 权重
     */
    @ApiModelProperty(value = "权重")
    @TableField(exist = false)
    private BigDecimal sumWeight=BigDecimal.ZERO;




}
