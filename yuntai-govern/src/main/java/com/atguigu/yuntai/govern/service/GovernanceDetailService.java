package com.atguigu.yuntai.govern.service;

import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 治理明细 服务类
 * </p>
 */
public interface GovernanceDetailService extends IService<GovernanceDetail> {

    /**
     *根据治理类型查询治理明细
     * @param optionType
     * @return
     */
    public List<GovernanceDetail> getGovernanceDetailListToFix(String optionType) ;

    /**
     * 根据治理类型查询治理明细
     * @param optionType
     * @return
     */
    public Map<String,List<GovernanceDetail>> getGovernanceDetailMapToFixByType(String optionType);
}
