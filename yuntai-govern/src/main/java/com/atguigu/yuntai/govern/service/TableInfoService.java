package com.atguigu.yuntai.govern.service;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 表元数据 服务接口
 * </p>
 *
 */
public interface TableInfoService extends IService<TableInfo> {

    /**
     *获取存储量
     * @return
     */
    public String getGlobalTableSize();
}
