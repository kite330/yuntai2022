package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 *有技术负责人评分器
 */
@Component("HAS_TEC_CHARGE_PERSON")
@Slf4j
public class OwnerTecJudger implements  Judger{

    //有技术负责人选项code
    public   final String OPTION_CODE="HAS_TEC_CHARGE_PERSON";
    //监控类型-规范
    public   final String OPTION_TYPE="SPEC";

    /**
     * 根据tableInfo中是否含有技术负责人进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo,String busiDate) {

        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);
        if(tableInfo.getTecOwnerUserId()!=null&&tableInfo.getTecOwnerUserId()>0){
            governanceDetail.setGovernanceContent("有技术负责人:"+tableInfo.getTecOwnerUserId() );
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }else{
            governanceDetail.setGovernanceContent("缺少技术负责人" );
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }

        return governanceDetail;
    }
}
