package com.atguigu.yuntai.govern.parser;

import lombok.Data;
import org.antlr.runtime.tree.Tree;

@Data
public class TableObj {

    public String tableAlias;

    public String tableName;

    public TableObj(Tree tree) {
        tableName = tree.getChild(0).getChild(0).getText();
        if (tree.getChildCount() > 1) {
            tableAlias = tree.getChild(1).getText();
        }

    }
}
