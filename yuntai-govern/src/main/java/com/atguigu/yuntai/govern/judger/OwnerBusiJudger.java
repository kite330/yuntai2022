package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 *有业务负责人评分器
 */
@Component("HAS_BUSI_CHARGE_PERSON")
@Slf4j
public class OwnerBusiJudger implements  Judger{

    //有业务负责人选项code
    public   final String OPTION_CODE="HAS_BUSI_CHARGE_PERSON";
    //质量监控-规范
    public   final String OPTION_TYPE="SPEC";

    /**
     * 根据表中是否存在业务负责人进行评分，如果存在分数赋值1，不存在则为0
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo,String busiDate) {

        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);

        if(tableInfo.getBusiOwnerUserId()!=null&&tableInfo.getBusiOwnerUserId()>0){
            governanceDetail.setGovernanceContent("有业务负责人:"+tableInfo.getBusiOwnerUserId() );
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }else{
            governanceDetail.setGovernanceContent("缺少业务负责人" );
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }

        return governanceDetail;
    }
}
