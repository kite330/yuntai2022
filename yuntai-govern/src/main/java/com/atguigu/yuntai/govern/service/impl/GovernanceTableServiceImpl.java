package com.atguigu.yuntai.govern.service.impl;

import com.atguigu.yuntai.govern.entity.GovernanceTable;
import com.atguigu.yuntai.govern.mapper.GovernanceTableMapper;
import com.atguigu.yuntai.govern.service.GovernanceTableService;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 表治理明细 服务实现类
 * </p>
 */
@Service
@DS("mysql-yuntai-config")
public class GovernanceTableServiceImpl extends ServiceImpl<GovernanceTableMapper, GovernanceTable> implements GovernanceTableService {

}
