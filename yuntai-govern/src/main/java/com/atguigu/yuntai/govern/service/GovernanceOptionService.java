package com.atguigu.yuntai.govern.service;

import com.atguigu.yuntai.govern.entity.GovernanceOption;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 治理项 服务接口
 * </p>
 */
public interface GovernanceOptionService extends IService<GovernanceOption> {
    /**
     * 查询治理项清单
     * @return
     */
    public Map<String, GovernanceOption> getGovernanceOptionMap();

    /**
     * 获取治理项的详细信息
     * @return
     */
    public  Map<String, List<GovernanceOption>> getGovernanceOptionGroupByType();
}
