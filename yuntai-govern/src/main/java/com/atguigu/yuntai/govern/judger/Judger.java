package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;

/**
 * 评分器接口
 */
public interface Judger {

    public GovernanceDetail judge(TableInfo tableInfo, String busiDate);
}
