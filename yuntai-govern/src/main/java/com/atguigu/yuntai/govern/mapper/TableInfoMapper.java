package com.atguigu.yuntai.govern.mapper;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 表元数据 Mapper 接口
 * </p>
 */
@DS("mysql-yuntai-config")
public interface TableInfoMapper extends BaseMapper<TableInfo> {

    /**
     * 查询表大小
     * @return
     */
    @Select("select sum(table_size) global_size  from table_info  ")
    public Long getGlobalTableSize();

}
