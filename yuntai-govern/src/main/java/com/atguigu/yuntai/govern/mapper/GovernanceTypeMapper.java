package com.atguigu.yuntai.govern.mapper;

import com.atguigu.yuntai.govern.entity.GovernanceType;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 治理项类别 Mapper 接口
 * </p>
 */
@DS("mysql-yuntai-config")
public interface GovernanceTypeMapper extends BaseMapper<GovernanceType> {

}
