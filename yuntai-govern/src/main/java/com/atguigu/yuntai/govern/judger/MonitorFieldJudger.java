package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import com.atguigu.yuntai.schedule.service.TDsTaskDefinitionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 是否有字段质量监控评分器
 */
@Component("HAS_MONITOR_FIELD")
@Slf4j
public class MonitorFieldJudger implements Judger {

    //字段质量监控code
    public final String OPTION_CODE = "HAS_MONITOR_FIELD";

    //监控类型-质量
    public final String OPTION_TYPE = "QUALITY";

    @Autowired
    TDsTaskDefinitionService tDsTaskDefinitionService;

    /**
     * 根据是否开启字段质量监控进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo, String busiDate) {

        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);

        if (1 == 1) {
            governanceDetail.setGovernanceContent("暂未开通配置");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        } else {
            governanceDetail.setGovernanceContent("暂未开通配置");
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }

        return governanceDetail;
    }
}
