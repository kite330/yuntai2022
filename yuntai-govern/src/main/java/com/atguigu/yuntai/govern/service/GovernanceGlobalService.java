package com.atguigu.yuntai.govern.service;

import com.atguigu.yuntai.govern.entity.GovernanceGlobal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 治理全局情况 服务类
 * </p>
 */
public interface GovernanceGlobalService extends IService<GovernanceGlobal> {

}
