package com.atguigu.yuntai.govern.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 表治理明细
 * </p>
 */
@Data
@ApiModel(description = "治理全局情况")
@TableName("governance_table")
public class GovernanceTable implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 业务日期
     */
    @ApiModelProperty(value = "业务日期")
    @TableField("busi_date")
    private String busiDate;

    /**
     * 表名
     */
    @ApiModelProperty(value = "表名")
    @TableField("table_name")
    private String tableName;

    /**
     * 库名
     */
    @ApiModelProperty(value = "统计日期")
    @TableField("schema_name")
    private String schemaName;

    /**
     * 技术负责人
     */
    @ApiModelProperty(value = "技术负责人")
    @TableField("tec_owner_user")
    private Long tecOwnerUserId;

    /**
     * 负责部门
     */
    @ApiModelProperty(value = "负责部门")
    @TableField("charge_dept_id")
    private Long chargeDeptId;

    /**
     * 规范分数
     */
    @ApiModelProperty(value = "统计日期")
    @TableField("score_spec")
    private BigDecimal scoreSpec = BigDecimal.ZERO;

    /**
     * 存储分数
     */
    @ApiModelProperty(value = "存储分数")
    @TableField("score_storage")
    private BigDecimal scoreStorage = BigDecimal.ZERO;

    /**
     * 计算分数
     */
    @ApiModelProperty(value = "计算分数")
    @TableField("score_calc")
    private BigDecimal scoreCalc = BigDecimal.ZERO;

    /**
     * 质量分数
     */
    @ApiModelProperty(value = "质量分数")
    @TableField("score_quality")
    private BigDecimal scoreQuality = BigDecimal.ZERO;

    /**
     * 安全分数
     */
    @ApiModelProperty(value = "安全分数")
    @TableField("score_security")
    private BigDecimal scoreSecurity = BigDecimal.ZERO;

    /**
     * 规范项个数
     */
    @ApiModelProperty(value = "规范项个数")
    @TableField("count_spec")
    private Long countSpec = 0L;

    /**
     * 存储项个数
     */
    @ApiModelProperty(value = "存储项个数")
    @TableField("count_storage")
    private Long countStorage = 0L;

    /**
     * 计算项个数
     */
    @ApiModelProperty(value = "计算项个数")
    @TableField("count_calc")
    private Long countCalc = 0L;

    /**
     * 质量项个数
     */
    @ApiModelProperty(value = "质量项个数")
    @TableField("count_quality")
    private Long countQuality = 0L;

    /**
     * 安全项个数
     */
    @ApiModelProperty(value = "安全项个数")
    @TableField("count_security")
    private Long countSecurity = 0L;

    /**
     * 五维权重后分数
     */
    @ApiModelProperty(value = "五维权重后分数")
    @TableField("score_with_type_weight")
    private BigDecimal scoreWithTypeWeight = BigDecimal.ZERO;

    /**
     * 被依赖次数
     */
    @ApiModelProperty(value = "被依赖次数")
    @TableField("be_dependent_count")
    private Long beDependentCount = 0L;

    /**
     * 表大小
     */
    @ApiModelProperty(value = "表大小")
    @TableField("table_size")
    private Long tableSize = 0L;

    /**
     * 权重
     */
    @ApiModelProperty(value = "权重")
    @TableField("table_weight")
    private BigDecimal tableWeight = BigDecimal.ZERO;

    /**
     * 问题项个数
     */
    @ApiModelProperty(value = "问题项个数")
    @TableField("problem_num")
    private Long problemNum = 0L;

    /**
     * 创建日期
     */
    @ApiModelProperty(value = "创建日期")
    @TableField("create_time")
    private Date createTime;

}
