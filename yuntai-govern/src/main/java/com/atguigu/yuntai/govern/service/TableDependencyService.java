package com.atguigu.yuntai.govern.service;

import com.atguigu.yuntai.govern.entity.TableDependency;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  表关系服务接口
 * </p>
 *
 */
public interface TableDependencyService extends IService<TableDependency> {

}
