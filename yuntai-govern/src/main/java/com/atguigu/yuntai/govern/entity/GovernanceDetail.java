package com.atguigu.yuntai.govern.entity;

import java.math.BigDecimal;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.baomidou.mybatisplus.annotation.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 治理明细
 * </p>
 */
@NoArgsConstructor
@Data
@ApiModel(description = "治理明细")
@TableName("governance_detail")
public class GovernanceDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    public GovernanceDetail(TableInfo tableInfo) {
        this.tableName = tableInfo.getTableName();
        this.schemaName = tableInfo.getSchemaName();
        this.tecOwnerUserId = tableInfo.getTecOwnerUserId();

    }

    /**
     * id
     */
    /**
     * id
     */
    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 业务日期
     */
    @ApiModelProperty(value = "业务日期")
    @TableField("busi_date")
    private String busiDate;

    /**
     * 表名
     */
    @ApiModelProperty(value = "表名")
    @TableField("table_name")
    private String tableName;

    /**
     * 库名
     */
    @ApiModelProperty(value = "库名")
    @TableField("schema_name")
    private String schemaName;

    /**
     * 治理项编码
     */
    @ApiModelProperty(value = "治理项编码")
    @TableField("option_code")
    private String optionCode;

    /**
     * 治理新类型
     */
    @ApiModelProperty(value = "治理新类型")
    @TableField("option_type")
    private String optionType;

    /**
     * 技术负责人
     */
    @ApiModelProperty(value = "技术负责人")
    @TableField("tec_owner_user_id")
    private Long tecOwnerUserId;

    /**
     * 治理项结果
     */
    @ApiModelProperty(value = "治理项结果")
    @TableField("governance_result")
    private String governanceResult;

    /**
     * 治理项得分
     */
    @ApiModelProperty(value = "治理项得分")
    @TableField("governance_score")
    private BigDecimal governanceScore = BigDecimal.ZERO;

    /**
     * 治理项内容
     */
    @ApiModelProperty(value = "治理项内容")
    @TableField("governance_content")
    private String governanceContent;

    /**
     * 治理项处理路径
     */
    @ApiModelProperty(value = "治理项处理路径")
    @TableField("governance_url")
    private String governanceUrl;

    /**
     * 创建日期
     */
    @ApiModelProperty(value = "创建日期")
    private Date createTime;

}
