package com.atguigu.yuntai.govern.service;

import com.atguigu.yuntai.govern.entity.TableInfo;

import java.util.List;

/**
 * 分数相关实现接口
 */
public interface ScoringService {

    public void calcScoring(List<TableInfo> tableInfoList, String busiDate);
}
