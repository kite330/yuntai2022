package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 是否15日产出评分器
 */
@Component("IS_OUTPUT_EMPTY_15D")
@Slf4j
public class TableOutPutEmptyJudger implements  Judger{

    //是否15日产出
    public   final String OPTION_CODE="IS_OUTPUT_EMPTY_15D";
    //计算
    public   final String OPTION_TYPE="CALC";

    /**
     * 根据是否15日产出进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo,String busiDate) {
        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);
        Date tableLastModifyTime = tableInfo.getTableLastModifyTime();
        if(tableLastModifyTime==null){
            governanceDetail.setGovernanceContent("未检测出最后修改日期");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
            return governanceDetail;
        }
        long lastModiMs = tableLastModifyTime.getTime();
        long nowMs = new Date().getTime();
        long betweenMs = nowMs - lastModiMs;
        long betweenDay  = betweenMs / 1000 / 3600 / 24;

        if (betweenDay<15){
            governanceDetail.setGovernanceContent("不存在15为产出的情况");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }else {
            governanceDetail.setGovernanceContent("15天未产出数据");
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }


        return governanceDetail;
    }
}
