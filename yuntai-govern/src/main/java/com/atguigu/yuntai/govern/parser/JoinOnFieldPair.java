package com.atguigu.yuntai.govern.parser;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JoinOnFieldPair {

    FieldObj fieldObj_1;
    FieldObj fieldObj_2;

}
