package com.atguigu.yuntai.govern.mapper;

import com.atguigu.yuntai.govern.entity.GovernanceUser;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 技术负责人治理明细 Mapper 接口
 */
@DS("mysql-yuntai-config")
public interface GovernanceUserMapper extends BaseMapper<GovernanceUser> {

}
