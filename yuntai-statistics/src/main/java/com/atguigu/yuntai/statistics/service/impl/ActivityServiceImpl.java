package com.atguigu.yuntai.statistics.service.impl;

import com.atguigu.yuntai.statistics.bean.QActivity;
import com.atguigu.yuntai.statistics.entity.AdsActivityStats;
import com.atguigu.yuntai.statistics.mapper.ActivityMapper;
import com.atguigu.yuntai.statistics.service.ActivityService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 活动分析实现类
 */

@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityMapper, AdsActivityStats> implements ActivityService {


    @Override
    public List<AdsActivityStats> getActivityNameByStartDate(QActivity qActivity) {

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.select("activity_name");
        queryWrapper.eq("dt", qActivity.getStartDate());
        queryWrapper.groupBy("activity_id");
        return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    public IPage<AdsActivityStats> selectPage(Page<AdsActivityStats> pageParam, QActivity qActivity) {
        return this.baseMapper.selectPage(pageParam, qActivity);
    }
}
