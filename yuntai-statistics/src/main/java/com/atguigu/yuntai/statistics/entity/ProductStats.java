package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Desc: 商品交易额统计实体类
 */
@Data
@Builder
@ApiModel(description = "各品牌商品交易统计")
@TableName("product_stats_2021")
public class ProductStats  implements Serializable {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "统计日期")
    @TableField("spu_id")
    Long spuId;
    @ApiModelProperty(value = "统计日期")
    @TableField("spu_name")
    String spuName;
    @ApiModelProperty(value = "统计日期")
    @TableField("order_amount")
    BigDecimal orderAmount ;

    @ApiModelProperty(value = "统计日期")
    @TableField("order_ct")
    @Builder.Default
    Long orderCt = 0L;

    @ApiModelProperty(value = "统计日期")
    @TableField("stt")
    String stt;
    @ApiModelProperty(value = "统计日期")
    @TableField("edt")
    String edt;
    @ApiModelProperty(value = "统计日期")
    @TableField("sku_id")
    Long skuId;
    @ApiModelProperty(value = "统计日期")
    @TableField("sku_name")
    String skuName;
    @ApiModelProperty(value = "统计日期")
    @TableField("sku_price")
    BigDecimal skuPrice;

    @ApiModelProperty(value = "统计日期")
    @TableField("tm_id")
    Long tmId;
    @ApiModelProperty(value = "统计日期")
    @TableField("tm_name")
    String tmName;
    @ApiModelProperty(value = "统计日期")
    @TableField("category3_id")
    Long category3Id;
    @ApiModelProperty(value = "统计日期")
    @TableField("category3_name")
    String category3Name;
    @ApiModelProperty(value = "统计日期")
    @TableField("display_ct")
    @Builder.Default
    Long displayCt = 0L;
    @ApiModelProperty(value = "统计日期")
    @TableField("click_ct")
    @Builder.Default
    Long clickCt = 0L;
    @ApiModelProperty(value = "统计日期")
    @TableField("cart_ct")
    @Builder.Default
    Long cartCt = 0L;
    @ApiModelProperty(value = "统计日期")
    @TableField("order_sku_num")
    @Builder.Default
    Long orderSkuNum = 0L;



    @ApiModelProperty(value = "统计日期")
    @TableField("payment_amount")
    @Builder.Default
    BigDecimal paymentAmount = BigDecimal.ZERO;
    @ApiModelProperty(value = "统计日期")
    @TableField("refund_ct")
    @Builder.Default
    Long refundCt = 0L;

    @ApiModelProperty(value = "统计日期")
    @TableField("refund_amount")
    @Builder.Default
    BigDecimal refundAmount = BigDecimal.ZERO;

    @ApiModelProperty(value = "统计日期")
    @TableField("comment_ct")
    @Builder.Default
    Long commentCt = 0L;

    @ApiModelProperty(value = "统计日期")
    @TableField("good_comment_ct")
    @Builder.Default
    Long goodCommentCt = 0L;

    @ApiModelProperty(value = "统计日期")
    @TableField("ts")
    Long ts;
}