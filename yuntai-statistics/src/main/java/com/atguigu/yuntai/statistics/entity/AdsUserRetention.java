package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;


@Data
@ApiModel(description = "用户留存率")
@TableName("ads_user_retention")
public class AdsUserRetention implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private Date dt;

    @ApiModelProperty(value = "用户新增日期")
    @TableField("create_date")
    private String createDate;

    @ApiModelProperty(value = "截至当前日期留存天数")
    @TableField("retention_day")
    private int retentionDay;

    @ApiModelProperty(value = "留存用户数量")
    @TableField("retention_count")
    private Long retentionCount;

    @ApiModelProperty(value = "新增用户数量")
    @TableField("new_user_count")
    private Long newUserCount;

    @ApiModelProperty(value = "留存率")
    @TableField("retention_rate")
    private BigDecimal retentionRate;

}
