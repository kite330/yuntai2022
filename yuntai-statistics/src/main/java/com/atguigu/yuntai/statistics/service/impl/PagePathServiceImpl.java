package com.atguigu.yuntai.statistics.service.impl;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsPagePath;
import com.atguigu.yuntai.statistics.mapper.PagePathMapper;
import com.atguigu.yuntai.statistics.service.PagePathService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 用户路径分析实现类
 */
@Service
public class PagePathServiceImpl extends ServiceImpl<PagePathMapper, AdsPagePath> implements PagePathService {


    /**
     * 用户路径分析
     *
     * @param qUnified
     * @return
     */
    @Override
    public Map getPagePathCount(QUnified qUnified) {
        //根据days和dt查询路径
        Map rsMap = new HashMap();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.select("source", "target", "path_count  value");
        queryWrapper.eq("recent_days", qUnified.getRecentDays());
        queryWrapper.eq("dt", qUnified.getDt());
        //遍历得到echart路径图所需要的数据格式
        List<Map> pagePathMapList = this.baseMapper.selectMaps(queryWrapper);
        if (CollectionUtils.isNotEmpty(pagePathMapList)) {
            HashSet<String> nodeSet = new HashSet();
            List<Map> nodeMapList = new ArrayList<>();
            for (Map pagePathMap : pagePathMapList) {
                String source = (String) pagePathMap.get("source");
                String target = (String) pagePathMap.get("target");
                nodeSet.add(source);
                nodeSet.add(target);
            }
            for (String nodeName : nodeSet) {
                Map<String, String> nodeMap = new HashMap<>();
                nodeMap.put("name", nodeName);
                nodeMapList.add(nodeMap);
            }
            rsMap.put("nodeData", nodeMapList);
            rsMap.put("linksData", pagePathMapList);
        }


        return rsMap;
    }
}
