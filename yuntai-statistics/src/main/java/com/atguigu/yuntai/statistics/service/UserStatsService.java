package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsUserStats;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 用户新增活跃实现接口
 */
public interface UserStatsService extends IService<AdsUserStats> {

    /**
     * 用户新增活跃以及变动统计
     *
     * @param qUnified
     * @return
     */
    Map getUser(QUnified qUnified);
}
