package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.entity.AdsUserRetention;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;


/**
 * 用户留存率服务接口
 */
public interface UserRetentionService extends IService<AdsUserRetention> {

    /**
     * 根据dt查询用户留存率
     *
     * @param dt
     * @return
     */
    List<Map> getUseRetention(String dt);
}
