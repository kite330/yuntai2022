package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.common.util.NameValueData;
import com.atguigu.yuntai.statistics.entity.AdsRepeatPurchaseByTm;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * 最近7/30日各品牌复购率Mapper
 */
@DS("mysql-gmall-report")
public interface RepeatPurchaseMapper extends BaseMapper<AdsRepeatPurchaseByTm> {
    @Select("  SELECT   tm_name name,order_repeat_rate value   " + "  FROM    ads_repeat_purchase_by_tm  " + "  WHERE    recent_days= #{recentDays}  and dt= #{curDate} limit #{showNum}")
    List<NameValueData> getTmRepeat(int recentDays, String curDate, int showNum);

    /**
     * 查询昨日各品牌复购率
     *
     * @return
     */
    @Select("SELECT dt,recent_days,tm_id,tm_name,order_repeat_rate FROM ads_repeat_purchase_by_tm WHERE TO_DAYS(NOW())-TO_DAYS(dt)=1")
    public List<AdsRepeatPurchaseByTm> getAdsRepeatPurchaseByTmYesterday();


    /**
     * 查询今日各品牌复购率
     *
     * @return
     */
    @Select("SELECT dt,recent_days,tm_id,tm_name,order_repeat_rate FROM ads_repeat_purchase_by_tm WHERE TO_DAYS(NOW())=TO_DAYS(dt)")
    public List<AdsRepeatPurchaseByTm> getAdsRepeatPurchaseByTmToday();

}
