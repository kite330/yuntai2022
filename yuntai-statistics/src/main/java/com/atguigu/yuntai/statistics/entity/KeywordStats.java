package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Desc: 关键词统计实体类
 */
@Data
@ApiModel(description = "各品牌商品交易统计")
@TableName("keyword_stats_2021")
public class KeywordStats implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("stt")
    private String stt;

    @ApiModelProperty(value = "统计日期")
    @TableField("edt")
    private String edt;

    @ApiModelProperty(value = "统计日期")
    @TableField("keyword")
    private String keyword;

    @ApiModelProperty(value = "统计日期")
    @TableField("ct")
    private Long ct;

    @ApiModelProperty(value = "统计日期")
    @TableField("ts")
    private String ts;
}
