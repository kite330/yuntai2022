package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.common.util.NameValueData;
import com.atguigu.yuntai.statistics.entity.AdsSkuCartNumTop;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 各分类商品购物车存量Top
 */
@DS("mysql-gmall-report")
public interface SkuCartNumTop3Mapper extends BaseMapper<AdsSkuCartNumTop> {

    @Select("  SELECT   category1_id value, category1_name name" + "  FROM    ads_sku_cart_num_top3_by_cate  " + "  WHERE   dt= #{curDate} group by category1_id")
    List<NameValueData> getCategory1(String curDate);


    @Select("  SELECT   category2_id value, category2_name name" + "  FROM    ads_sku_cart_num_top3_by_cate  " + "  WHERE   dt= #{curDate} and category1_id = #{category1_id} group by category2_id")
    List<NameValueData> getCategory2(String curDate, String category1_id);

    @Select("  SELECT   category3_id value, category3_name name" + "  FROM    ads_sku_cart_num_top3_by_cate  " + "  WHERE   dt= #{curDate} and category1_id = #{category1_id} and category2_id=#{category2_id}" + " group by category3_id")
    List<NameValueData> getCategory3(String curDate, String category1_id, String category2_id);

    @Select("  SELECT   sku_name name, cart_num value" + "  FROM    ads_sku_cart_num_top3_by_cate  " + "  WHERE   dt= #{curDate} and category1_id = #{category1_id} and category2_id=#{category2_id} " + "and category3_id=#{category3_id} order by rk asc")
    List<NameValueData> getTmTopNData(String curDate, String category1_id, String category2_id, String category3_id);
}
