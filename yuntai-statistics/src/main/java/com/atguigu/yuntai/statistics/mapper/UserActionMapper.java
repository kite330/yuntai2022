package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.entity.AdsUserAction;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户行为漏斗分析Mapper
 */
@DS("mysql-gmall-report")
public interface UserActionMapper extends BaseMapper<AdsUserAction> {


    /**
     * 查询昨日用户行为
     *
     * @return
     */
    @Select("SELECT dt,recent_days,home_count,good_detail_count,cart_count,order_count,payment_count FROM ads_user_action WHERE TO_DAYS(NOW())-TO_DAYS(dt)=1")
    public List<AdsUserAction> getAdsUserActionYesterday();

    @Select("SELECT dt,recent_days,home_count,good_detail_count,cart_count,order_count,payment_count FROM ads_user_action WHERE TO_DAYS(NOW())=TO_DAYS(dt)")
    public List<AdsUserAction> getAdsUserActionToday();
}
