package com.atguigu.yuntai.statistics.bean;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Desc: 访客流量统计返回值实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "各品牌商品交易统计")
public class ReturnVisitorStats  implements Serializable {
    private static final long serialVersionUID = 1L;

    private String type;

    private Long newCount;

    private Long oldCount;
}
