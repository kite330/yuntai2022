package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.common.util.NameValueData;
import com.atguigu.yuntai.statistics.entity.AdsTradeStatsByCate;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * 各品类商品交易 Mapper
 */
@DS("mysql-gmall-report")
public interface TradeStatsCateMapper extends BaseMapper<AdsTradeStatsByCate> {
    @Select("  SELECT   *   " + "  FROM    ads_trade_stats_by_cate  " + "  WHERE    recent_days= #{recentDays}  and dt= #{curDate} group by category1_id")
    List<AdsTradeStatsByCate> getCategory1List(int recentDays, String curDate);


    @Select("  SELECT   category2_id,category2_name,order_count orderCountTotal   " + "  FROM    ads_trade_stats_by_cate  " + "  WHERE    recent_days= #{recentDays}  and dt= #{curDate} and category1_id =#{category1_id} group by category2_id")
    List<AdsTradeStatsByCate> getCategory2List(int recentDays, String curDate, String category1_id);

    @Select("  SELECT   category3_name name,order_count value" + "  FROM    ads_trade_stats_by_cate  " + "  WHERE    recent_days= #{recentDays}  and dt= #{curDate} " + "and category1_id =#{category1_id} and category2_id=#{category2_id}")
    List<NameValueData> getCategory3List(int recentDays, String curDate, String category1_id, String category2_id);

    /**
     * 查询昨日各品类商品交易统计
     *
     * @return
     */
    @Select("SELECT dt,recent_days,category1_id,category1_name,category2_id,category2_name,category3_id,category3_name,order_count,order_user_count,order_refund_count,order_refund_user_count FROM ads_trade_stats_by_cate WHERE TO_DAYS(NOW())-TO_DAYS(dt)=1")
    public List<AdsTradeStatsByCate> getAdsTradeStatsByCateYesterday();

    /**
     * 查询今日各类商品交易统计
     *
     * @return
     */
    @Select("SELECT dt,recent_days,category1_id,category1_name,category2_id,category2_name,category3_id,category3_name,order_count,order_user_count,order_refund_count,order_refund_user_count FROM ads_trade_stats_by_cate WHERE TO_DAYS(NOW())=TO_DAYS(dt)")
    public List<AdsTradeStatsByCate> getAdsTradeStatsByCateToday();


}
