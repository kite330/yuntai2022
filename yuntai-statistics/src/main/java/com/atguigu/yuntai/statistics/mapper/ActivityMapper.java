package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.bean.QActivity;
import com.atguigu.yuntai.statistics.entity.AdsActivityStats;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 最近30天发布的活动的补贴率 Mapper
 */
@DS("mysql-gmall-report")
public interface ActivityMapper extends BaseMapper<AdsActivityStats> {
    @Select("select * from ads_activity_stats  where dt = #{vo.startDate} and activity_name= #{vo.activityName}")
    IPage<AdsActivityStats> selectPage(Page<AdsActivityStats> page, @Param("vo") QActivity qActivity);
}
