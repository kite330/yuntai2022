package com.atguigu.yuntai.statistics.service.impl;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsNewBuyerStats;
import com.atguigu.yuntai.statistics.mapper.UserNewBuyerStatsMapper;
import com.atguigu.yuntai.statistics.service.UserNewBuyerStatsService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 新增交易用户统计实现类
 */
@Service
public class UserNewBuyerStatsServiceImpl extends ServiceImpl<UserNewBuyerStatsMapper, AdsNewBuyerStats> implements UserNewBuyerStatsService {

    @Autowired
    private UserNewBuyerStatsMapper userNewBuyerStatsMapper;

    @Override
    public AdsNewBuyerStats getNewBuyerStats(QUnified qUnified) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("recent_days", qUnified.getRecentDays());
        queryWrapper.eq("dt", qUnified.getDt());
        return userNewBuyerStatsMapper.selectOne(queryWrapper);
    }
}
