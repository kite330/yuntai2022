package com.atguigu.yuntai.statistics.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.atguigu.yuntai.common.util.EchartData;
import com.atguigu.yuntai.common.util.NameValueData;
import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsUserAction;
import com.atguigu.yuntai.statistics.mapper.UserActionMapper;
import com.atguigu.yuntai.statistics.service.UserActionService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

@Service
public class UserActionServiceImpl extends ServiceImpl<UserActionMapper, AdsUserAction> implements UserActionService {



    @Override
    public EchartData getUserActionConvert(QUnified qUnified) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("recent_days", qUnified.getRecentDays());
        queryWrapper.eq("dt", qUnified.getDt());
        AdsUserAction adsUserAction = this.baseMapper.selectOne(queryWrapper);
        JSONArray xDataList = new JSONArray(Arrays.asList("浏览首页人数", "浏览商品详情页人数", "加入购物车人数", "下单人数", "支付人数"));
        JSONArray yDataList = new JSONArray();
        if (adsUserAction != null) {
            BigDecimal homeCount = BigDecimal.valueOf(adsUserAction.getHomeCount());
            BigDecimal detailCount = BigDecimal.valueOf(adsUserAction.getGoodDetailCount());
            BigDecimal cartCount = BigDecimal.valueOf(adsUserAction.getCartCount());
            BigDecimal orderCount = BigDecimal.valueOf(adsUserAction.getOrderCount());
            BigDecimal payCount = BigDecimal.valueOf(adsUserAction.getPaymentCount());
            //计算百分比
            yDataList.add(new NameValueData("浏览首页人数", BigDecimal.valueOf(100)));
            yDataList.add(new NameValueData("浏览商品详情页人数", detailCount.divide(homeCount, 3, RoundingMode.HALF_UP).movePointRight(2)));
            yDataList.add(new NameValueData("加入购物车人数", cartCount.divide(homeCount, 3, RoundingMode.HALF_UP).movePointRight(2)));
            yDataList.add(new NameValueData("下单人数", orderCount.divide(homeCount, 3, RoundingMode.HALF_UP).movePointRight(2)));
            yDataList.add(new NameValueData("支付人数", payCount.divide(homeCount, 3, RoundingMode.HALF_UP).movePointRight(2)));

        }
        return new EchartData(xDataList, yDataList);
    }

}
