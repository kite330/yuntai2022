package com.atguigu.yuntai.statistics.service;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.yuntai.common.util.NameValueData;
import com.atguigu.yuntai.statistics.entity.AdsSkuCartNumTop;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 各分类商品购物车存量Top服务接口
 */
public interface SkuCartNumService extends IService<AdsSkuCartNumTop> {

    /**
     * 查询各分类商品购物车存量中一级分类
     *
     * @param curDate
     * @return
     */
    List<NameValueData> getCategory1(String curDate);

    /**
     * 查询各分类商品购物车存量中二级分类
     *
     * @param curDate
     * @param category1Id
     * @return
     */
    List<NameValueData> getCategory2(String curDate, String category1Id);

    /**
     * 查询各分类商品购物车存量中三级分类
     *
     * @param curDate
     * @param category1Id
     * @param category2Id
     * @return
     */
    List<NameValueData> getCategory3(String curDate, String category1Id, String category2Id);

    /**
     * 查询各分类商品购物车存量Top3
     *
     * @param curDate
     * @param category1Id
     * @param category2Id
     * @param category3Id
     * @return
     */
    JSONObject getTmTopNData(String curDate, String category1Id, String category2Id, String category3Id);
}
