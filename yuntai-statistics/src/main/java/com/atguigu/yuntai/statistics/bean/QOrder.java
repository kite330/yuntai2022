package com.atguigu.yuntai.statistics.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: gmall
 * @description:订单查询实体类
 */
@Data
@ApiModel(description = "订单查询")
public class QOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "开始日期")
    private String startTime;

    @ApiModelProperty(value = "结束日期")
    private String endTime;


}
