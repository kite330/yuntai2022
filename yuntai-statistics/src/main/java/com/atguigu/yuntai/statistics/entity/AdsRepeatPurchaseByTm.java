package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;


@Data
@ApiModel(description = "最近7/30日各品牌复购率")
@TableName("ads_repeat_purchase_by_tm")
public class AdsRepeatPurchaseByTm  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private Date dt;

    @ApiModelProperty(value = "最近天数,7:最近7天,30:最近30天")
    @TableField("recent_days")
    private long recentDays;

    @ApiModelProperty(value = "品牌Id")
    @TableField("tm_id")
    private String tmId;

    @ApiModelProperty(value = "品牌名称")
    @TableField("tm_name")
    private String tmName;

    @ApiModelProperty(value = "复购率")
    @TableField("order_repeat_rate")
    private BigDecimal orderRepeatRate;


}
