package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;


@Data
@ApiModel(description = "用户新增活跃统计")
@TableName("ads_user_stats")
public class AdsUserStats implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private Date dt;

    @ApiModelProperty(value = "最近n日,1:最近1日,7:最近7日,30:最近30日")
    @TableField("recent_days")
    private long recentDays;

    @ApiModelProperty(value = "新增用户数")
    @TableField("new_user_count")
    private Long newUserCount;

    @ApiModelProperty(value = "活跃用户数")
    @TableField("active_user_count")
    private Long activeUserCount;


}
