package com.atguigu.yuntai.statistics.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: gmall
 * @description:活动查询字段实体类
 */

@Data
@ApiModel(description = "活动查询字段")
public class QActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "活动开始日期")
    private String startDate;

    @ApiModelProperty(value = "活动名称")
    private String activityName;

}
