package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsTradeStatsByTm;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 各品牌商品交易统计服务接口
 */
public interface TradeStatsTmService extends IService<AdsTradeStatsByTm> {

    /**
     * 各品牌商品交易统计
     *
     * @param pageParam
     * @param qUnified
     * @return
     */
    IPage<AdsTradeStatsByTm> selectPage(Page<AdsTradeStatsByTm> pageParam, QUnified qUnified);
}
