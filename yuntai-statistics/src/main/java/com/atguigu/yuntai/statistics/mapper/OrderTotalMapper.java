package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.entity.AdsOrderByProvince;
import com.atguigu.yuntai.statistics.entity.AdsTradeStats;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@DS("mysql-gmall-report")
public interface OrderTotalMapper extends BaseMapper<AdsOrderByProvince> {


    @Select("select * from ads_trade_stats where dt = #{dt} and recent_days = #{days}")
    AdsTradeStats getTradeByDaysAndDt(int days, String dt);

    @Select("select province_name,order_count from ads_order_by_province where dt = #{dt} and recent_days = #{days} order by order_count ASC;")
    List<AdsOrderByProvince> getOrderProvinceCount(int days, String dt);

    @Select("select province_name,order_total_amount from ads_order_by_province where dt = #{dt} and recent_days = #{days} order by order_total_amount ASC;")
    List<AdsOrderByProvince> getOrderProvinceTotalAmount(int days, String dt);
}
