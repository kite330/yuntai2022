package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Desc: 地区交易额统计实体类
 */
@Data
@ApiModel(description = "各品牌商品交易统计")
@TableName("province_stats_2021")
public class ProvinceStats implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("stt")
    private String stt;

    @ApiModelProperty(value = "统计日期")
    @TableField("edt")
    private String edt;

    @ApiModelProperty(value = "统计日期")
    @TableField("province_id")
    private String provinceId;

    @ApiModelProperty(value = "统计日期")
    @TableField("province_name")
    private String provinceName;

    @ApiModelProperty(value = "统计日期")
    @TableField("order_amount")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "统计日期")
    @TableField("ts")
    private String ts;
}
