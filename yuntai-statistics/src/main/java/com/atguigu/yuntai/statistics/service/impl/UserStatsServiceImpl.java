package com.atguigu.yuntai.statistics.service.impl;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsNewBuyerStats;
import com.atguigu.yuntai.statistics.entity.AdsUserChange;
import com.atguigu.yuntai.statistics.entity.AdsUserStats;
import com.atguigu.yuntai.statistics.mapper.UserStatsMapper;
import com.atguigu.yuntai.statistics.service.UserChangeService;
import com.atguigu.yuntai.statistics.service.UserNewBuyerStatsService;
import com.atguigu.yuntai.statistics.service.UserStatsService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 用户新增活跃实现类
 */
@Service
public class UserStatsServiceImpl extends ServiceImpl<UserStatsMapper, AdsUserStats> implements UserStatsService {

    @Autowired
    private UserStatsMapper userStatsMapper;
    @Autowired
    private UserChangeService userChangeService;
    @Autowired
    private UserNewBuyerStatsService userNewBuyerStatsService;


    @Override
    public Map getUser(QUnified qUnified) {
        Map userTotalMap = new HashMap();
        //查询用户新增活跃
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("recent_days", qUnified.getRecentDays());
        queryWrapper.eq("dt", qUnified.getDt());
        AdsUserStats adsUserStats = userStatsMapper.selectOne(queryWrapper);
        //查询用户变动
        AdsUserChange userChange = userChangeService.getUserChange(qUnified);
        //查询新增交易用户
        AdsNewBuyerStats newBuyerStats = userNewBuyerStatsService.getNewBuyerStats(qUnified);
        if (adsUserStats != null) {
            userTotalMap.put("userTotal", adsUserStats.getNewUserCount());
            userTotalMap.put("activeUserTotal", adsUserStats.getActiveUserCount());
        }
        if (userChange != null) {
            userTotalMap.put("userChurn", userChange.getUserChurnCount());
            userTotalMap.put("userBack", userChange.getUserBackCount());
        }

        if (newBuyerStats != null) {
            userTotalMap.put("newPaymentUserCount", newBuyerStats.getNewPaymentUserCount());
            userTotalMap.put("newOrderUserCount", newBuyerStats.getNewOrderUserCount());
        }
        return userTotalMap;
    }
}
