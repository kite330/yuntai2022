package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Desc: 访客流量统计实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "各品牌商品交易统计")
@TableName("visitor_stats_2021")
public class VisitorStats  implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("stt")
    private String stt;

    @ApiModelProperty(value = "统计日期")
    @TableField("edt")
    private String edt;

    @ApiModelProperty(value = "统计日期")
    @TableField("vc")
    private String vc;

    @ApiModelProperty(value = "统计日期")
    @TableField("ch")
    private String ch;

    @ApiModelProperty(value = "统计日期")
    @TableField("ar")
    private String ar;

    @ApiModelProperty(value = "统计日期")
    @TableField("is_new")
    private String isNew;

    @ApiModelProperty(value = "统计日期")
    @TableField("uv_ct")
    private Long uvCt = 0L;

    @ApiModelProperty(value = "统计日期")
    @TableField("pv_ct")
    private Long pvCt = 0L;

    @ApiModelProperty(value = "统计日期")
    @TableField("sv_ct")
    private Long svCt = 0L;

    @ApiModelProperty(value = "统计日期")
    @TableField("uj_ct")
    private Long ujCt = 0L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dur_sum")
    private Long durSum = 0L;

    @ApiModelProperty(value = "统计日期")
    @TableField("new_uv")
    private Long newUv = 0L;

    @ApiModelProperty(value = "统计日期")
    @TableField("ts")
    private Long ts;

    @ApiModelProperty(value = "统计日期")
    @TableField("hr")
    private int hr;

    //计算跳出率  = 跳出次数*100/访问次数
    public BigDecimal getUjRate() {
        if (uvCt != 0L) {
            return BigDecimal.valueOf(ujCt)
                    .multiply(BigDecimal.valueOf(100))
                    .divide(BigDecimal.valueOf(svCt), 2, RoundingMode.HALF_UP);
        } else {
            return BigDecimal.ZERO;
        }
    }

    //计算每次访问停留时间(秒)  = 当日总停留时间（毫秒)/当日访问次数/1000
    public BigDecimal getDurPerSv() {
        if (uvCt != 0L) {
            return BigDecimal.valueOf(durSum)
                    .divide(BigDecimal.valueOf(svCt), 0, RoundingMode.HALF_UP)
                    .divide(BigDecimal.valueOf(1000), 1, RoundingMode.HALF_UP);
        } else {
            return BigDecimal.ZERO;
        }
    }

    //计算每次访问停留页面数 = 当日总访问页面数/当日访问次数
    public BigDecimal getPvPerSv() {
        if (uvCt != 0L) {
            return BigDecimal.valueOf(pvCt)
                    .divide(BigDecimal.valueOf(svCt), 2, RoundingMode.HALF_UP);
        } else {
            return BigDecimal.ZERO;
        }
    }
}
