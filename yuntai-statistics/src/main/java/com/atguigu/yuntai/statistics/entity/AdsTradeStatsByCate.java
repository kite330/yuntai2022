package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 各品类商品交易统计实体类
 */
@Data
@ApiModel(description = "各品类商品交易统计")
@TableName("ads_trade_stats_by_cate")
public class AdsTradeStatsByCate  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private String dt;

    /**
     * 最近天数,1:最近1天,7:最近7天,30:最近30天
     */
    @ApiModelProperty(value = "最近天数")
    @TableField("recent_days")
    private int recentDays;


    @ApiModelProperty(value = "一级分类id")
    @TableField("category1_id")
    private String category1Id;


    @ApiModelProperty(value = "一级分类名称")
    @TableField("category1_name")
    private String category1Name;


    @ApiModelProperty(value = "二级分类id")
    @TableField("category2_id")
    private String category2Id;


    @ApiModelProperty(value = "二分类名称")
    @TableField("category2_name")
    private String category2Name;


    @ApiModelProperty(value = "三级分类id")
    @TableField("category3_id")
    private String category3Id;


    @ApiModelProperty(value = "三级分类名称")
    @TableField("category3_name")
    private String category3Name;


    @ApiModelProperty(value = "订单数")
    @TableField("order_count")
    private int orderCount;

    @ApiModelProperty(value = "订单人数")
    @TableField("order_user_count")
    private int orderUserCount;


    @ApiModelProperty(value = "退单数")
    @TableField("order_refund_count")
    private int orderRefundCount;


    @ApiModelProperty(value = "退单人数")
    @TableField("order_refund_user_count")
    private int orderRefundUserCount;

    @ApiModelProperty(value = "订单总数")
    @TableField(exist = false)
    private int orderCountTotal;
}
