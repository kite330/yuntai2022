package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsUserChange;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户变动统计服务接口
 */
public interface UserChangeService extends IService<AdsUserChange> {

    /**
     * 根据dt查询用户变动
     *
     * @param qUnified
     * @return
     */
    AdsUserChange getUserChange(QUnified qUnified);
}
