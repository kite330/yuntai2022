package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsPagePath;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 用户路径分析服务接口
 */
public interface PagePathService extends IService<AdsPagePath> {

    /**
     * 用户路径分析
     *
     * @param qUnified
     * @return
     */
    Map getPagePathCount(QUnified qUnified);
}
