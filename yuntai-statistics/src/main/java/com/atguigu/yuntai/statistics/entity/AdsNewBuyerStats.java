package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;


@Data
@ApiModel(description = "新增交易用户统计")
@TableName("ads_new_buyer_stats")
public class AdsNewBuyerStats implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private Date dt;

    @ApiModelProperty(value = "最近天数")
    @TableField("recent_days")
    private long recentDays;

    @ApiModelProperty(value = "新增下单人数")
    @TableField("new_order_user_count")
    private Long newOrderUserCount;

    @ApiModelProperty(value = "新增支付人数")
    @TableField("new_payment_user_count")
    private Long newPaymentUserCount;

}
