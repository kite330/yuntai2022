package com.atguigu.yuntai.statistics.service.impl;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsTradeStats;
import com.atguigu.yuntai.statistics.mapper.TradeStatsMapper;
import com.atguigu.yuntai.statistics.service.TradeStatsService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 交易综合统计实现类
 */
@Service
public class TradeStatsServiceImpl extends ServiceImpl<TradeStatsMapper, AdsTradeStats> implements TradeStatsService {



    @Override
    public AdsTradeStats getTradeByDaysAndDt(QUnified qUnified) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("recent_days", qUnified.getRecentDays());
        queryWrapper.eq("dt", qUnified.getDt());
        return this.baseMapper.selectOne(queryWrapper);
    }
}
