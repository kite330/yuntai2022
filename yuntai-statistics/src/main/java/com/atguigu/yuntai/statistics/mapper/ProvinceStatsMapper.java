package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.entity.ProductStats;
import com.atguigu.yuntai.statistics.entity.ProvinceStats;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Desc:  地区维度统计Mapper
 */
@DS("clickhouse")
public interface ProvinceStatsMapper extends BaseMapper<ProvinceStats> {
    //按地区查询交易额
    @Select("select province_name,sum(order_amount) order_amount " +
            "from  province_stats_2021 where toYYYYMMDD(stt)=#{date} " +
            "group by province_id ,province_name  ")
    public List<ProvinceStats> selectProvinceStats(int date);
}
