package com.atguigu.yuntai.statistics.service.impl;

import com.atguigu.yuntai.statistics.bean.QCoupon;
import com.atguigu.yuntai.statistics.entity.AdsActivityStats;
import com.atguigu.yuntai.statistics.entity.AdsCouponStats;
import com.atguigu.yuntai.statistics.mapper.ActivityMapper;
import com.atguigu.yuntai.statistics.mapper.CouponMapper;
import com.atguigu.yuntai.statistics.service.CouponService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 优惠券统计实现类
 */

@Service
public class CouponServiceImpl extends ServiceImpl<CouponMapper, AdsCouponStats> implements CouponService {


    @Override
    public List<AdsCouponStats> getCouponNameByStartDate(QCoupon qCoupon) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.select("coupon_name");
        queryWrapper.eq("dt", qCoupon.getStartDate());
        queryWrapper.groupBy("coupon_id");
        return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    public IPage<AdsCouponStats> selectPage(Page<AdsCouponStats> pageParam, QCoupon qCoupon) {
        return this.baseMapper.selectPage(pageParam, qCoupon);
    }
}
