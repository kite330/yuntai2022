package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.entity.AdsOrderByProvince;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 各省份交易统计Mapper
 */
@DS("mysql-gmall-report")
public interface OrderProvinceMapper extends BaseMapper<AdsOrderByProvince> {
    /**
     * 查询昨日各省份订单统计
     *
     * @return
     */
    @Select("SELECT dt,recent_days,province_id,province_name,area_code,iso_code,iso_code_3166_2,order_count,order_total_amount FROM ads_order_by_province WHERE TO_DAYS(NOW())-TO_DAYS(dt)=1")
    public List<AdsOrderByProvince> getAdsOrderByProvinceYesterday();


    /**
     * 查询今日各省份订单统计
     *
     * @return
     */
    @Select("SELECT dt,recent_days,province_id,province_name,area_code,iso_code,iso_code_3166_2,order_count,order_total_amount FROM ads_order_by_province WHERE TO_DAYS(NOW())=TO_DAYS(dt);")
    public List<AdsOrderByProvince> getAdsOrderByProvinceToday();
}
