package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 最近30天发布的优惠卷的补贴率实体类
 */
@Data
@ApiModel(description = "最近30天发布的优惠卷的补贴率")
@TableName("ads_coupon_stats")
public class AdsCouponStats  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private String dt;


    @ApiModelProperty(value = "优惠券ID")
    @TableField("coupon_id")
    private String couponId;


    @ApiModelProperty(value = "优惠券名称")
    @TableField("coupon_name")
    private String couponName;


    @ApiModelProperty(value = "发布日期")
    @TableField("start_date")
    private String startDate;


    @ApiModelProperty(value = "优惠规则")
    @TableField("rule_name")
    private String ruleName;


    @ApiModelProperty(value = "补贴率")
    @TableField("reduce_rate")
    private BigDecimal reduceRate;
}
