package com.atguigu.yuntai.lineage.bean;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class EchartNode {


    int x ;
    int y ;
    String name;

    Integer symbolSize =null;
    Map itemStyle =null;
}
