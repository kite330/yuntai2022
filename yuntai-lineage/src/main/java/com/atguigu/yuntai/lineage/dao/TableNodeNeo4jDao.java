package com.atguigu.yuntai.lineage.dao;


import com.atguigu.yuntai.lineage.bean.TableNode;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
//import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface TableNodeNeo4jDao extends Neo4jRepository<TableNode,String> {

         @Query("MATCH (a)-[r*0..]->(n:TableNode{name: {tableName} }) -[l*0..]->(x)  RETURN  a,r,n,l,x LIMIT 25")
        public List<TableNode> matchLineageTableNode(@Param("tableName") String tableName);

}
