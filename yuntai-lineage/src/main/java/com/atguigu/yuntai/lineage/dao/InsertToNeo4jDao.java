package com.atguigu.yuntai.lineage.dao;



import com.atguigu.yuntai.lineage.bean.Insert;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public interface InsertToNeo4jDao extends Neo4jRepository<Insert,Long> {


    @Query("MATCH (a)-[r*0..]->(n:TableNode{name: {tableName} }) -[l*0..]->(x)  RETURN  a,r,n,l,x LIMIT 25")
    public List<Insert> matchLineageTableNode(@Param("tableName") String tableName);

}
