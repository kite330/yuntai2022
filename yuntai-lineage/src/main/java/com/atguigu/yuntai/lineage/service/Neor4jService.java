package com.atguigu.yuntai.lineage.service;

import com.atguigu.yuntai.lineage.bean.Insert;

import java.util.List;

public interface Neor4jService {



    public  List<Insert>  matchLineage(String tableName);

    public void saveInsertList(List<Insert> insertList);
}
