package com.atguigu.yuntai.lineage.service.impl;

import com.atguigu.yuntai.lineage.bean.Insert;
import com.atguigu.yuntai.lineage.bean.TableDep;
import com.atguigu.yuntai.lineage.bean.TableNode;

import com.atguigu.yuntai.lineage.dao.InsertToNeo4jDao;
import com.atguigu.yuntai.lineage.dao.TableNodeNeo4jDao;
import com.atguigu.yuntai.lineage.service.Neor4jService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class Neo4jServiceImpl implements Neor4jService {

    @Autowired
    TableNodeNeo4jDao tableNodeNeo4JDao;

    @Autowired
    InsertToNeo4jDao insertToNeo4JDao;

    public  void saveTableNode(){


        TableNode orderInfoNode = new TableNode("dwd_order_info", "dwd_order_info" );
        TableNode orderDetailNode = new TableNode("dwd_order_detail", "dwd_order_detail" );
        TableNode cartNode = new TableNode("dwd_cart_info", "dwd_cart_info" );
        TableNode odsNode = new TableNode("ods_base_db", "ods_base_db" );


        TableNode orderWideNode = new TableNode("dws_order_wide", "dws_order_wide");
//        tableNode.addSourceNode(orderInfoNode);
//        tableNode.addSourceNode(orderDetailNode);
        Insert insert = new Insert(orderInfoNode, orderWideNode);
        Insert insert2 = new Insert(odsNode, cartNode);
        Insert insert3 = new Insert(odsNode, orderInfoNode);
        Insert insert4 = new Insert(odsNode, orderDetailNode);
//        tableNodeNeo4JMapper.deleteAll();
        tableNodeNeo4JDao.save(orderInfoNode);
        tableNodeNeo4JDao.save(orderDetailNode);
        insertToNeo4JDao.save(insert);   //可以直接存关联
        insertToNeo4JDao.save(insert2);
        insertToNeo4JDao.save(insert3);
        insertToNeo4JDao.save(insert4);
    }

    public  List<Insert>  matchLineage(String tableName){
        List<Insert> insertList = insertToNeo4JDao.matchLineageTableNode(tableName);
        return insertList;
    }



    public void saveInsertList(List<Insert> insertList){
        tableNodeNeo4JDao.deleteAll();
        insertToNeo4JDao.deleteAll();
        insertToNeo4JDao.save(insertList,2);
    }

    void saveTableDeps(List<TableDep> tableDepList){
        //   Map

        for (TableDep tableDep : tableDepList) {
            TableNode tableNode = new TableNode();
            tableNode.setName(tableDep.getTableTargetName());
            Set<String> tableSourceNameSet = tableDep.getTableSourceNameSet();
            //  Insert insertTo = new Insert();
            //   insertTo.setTableNode(new TableNode());
        }



    }
}
