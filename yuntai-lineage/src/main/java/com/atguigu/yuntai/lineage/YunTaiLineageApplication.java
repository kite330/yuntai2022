//package com.atguigu.yuntai.lineage;
//
//import org.mybatis.spring.annotation.MapperScan;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;
//
//@SpringBootApplication
////@EnableSwagger2
//@ComponentScan(basePackages = {"com.atguigu.yuntai"})
//@MapperScan(basePackages = {"com.atguigu.yuntai.schedule.mapper"})
//@EnableNeo4jRepositories(basePackages = {"com.atguigu.yuntai.lineage.dao"})
////@Configuration("com.atguigu.yuntai.monitor.config")
//public class YunTaiLineageApplication {
//
//    public static void main(String[] args) {
//        SpringApplication.run(YunTaiLineageApplication.class, args);
//    }
//
//}
