package com.atguigu.yuntai.lineage.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@AllArgsConstructor
@Data
public class TableDep {

    Set<String> tableSourceNameSet;

    String tableTargetName;


}
