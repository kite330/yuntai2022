/*
 Navicat Premium Data Transfer

 Source Server         : aliyun-yuntai
 Source Server Type    : MySQL
 Source Server Version : 50716
 Source Host           : aliyun102:3306
 Source Schema         : yuntai_acl

 Target Server Type    : MySQL
 Target Server Version : 50716
 File Encoding         : 65001

 Date: 01/08/2022 15:58:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '会员id',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `phone` varchar(11) DEFAULT NULL COMMENT '手机',
  `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库id（默认为：0为平台用户）',
  `create_id` bigint(20) DEFAULT NULL,
  `create_name` varchar(20) DEFAULT NULL,
  `update_id` bigint(20) DEFAULT NULL,
  `update_name` varchar(20) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标记（0:不可用 1:可用）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uname` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of admin
-- ----------------------------
BEGIN;
INSERT INTO `admin` VALUES (1, 'admin', '96e79218965eb72c92a549dd5a330112', 'admin', '1501111111', NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:08:43', '2022-04-06 10:20:09', 0);
INSERT INTO `admin` VALUES (2, 'zhangsan', '96e79218965eb72c92a549dd5a330112', '张散', '15010456547', NULL, NULL, NULL, NULL, NULL, '2022-03-31 09:54:14', '2022-07-12 09:57:57', 1);
INSERT INTO `admin` VALUES (3, 'beijing', '96e79218965eb72c92a549dd5a330112', '李北京', '15010908900', 1, NULL, NULL, 1, 'admin', '2022-04-06 09:41:55', '2022-04-06 09:41:55', 0);
COMMIT;

-- ----------------------------
-- Table structure for admin_login_log
-- ----------------------------
DROP TABLE IF EXISTS `admin_login_log`;
CREATE TABLE `admin_login_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) DEFAULT NULL,
  `ip` varchar(64) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `user_agent` varchar(100) DEFAULT NULL COMMENT '浏览器登录类型',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标记（0:不可用 1:可用）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户登录日志表';

-- ----------------------------
-- Records of admin_login_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for admin_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色id',
  `admin_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户id',
  `create_id` bigint(20) DEFAULT NULL,
  `create_name` varchar(20) DEFAULT NULL,
  `update_id` bigint(20) DEFAULT NULL,
  `update_name` varchar(20) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标记（0:不可用 1:可用）',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_user_id` (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='用户角色';

-- ----------------------------
-- Records of admin_role
-- ----------------------------
BEGIN;
INSERT INTO `admin_role` VALUES (1, 1, 1, NULL, NULL, NULL, NULL, '2021-05-31 18:09:02', '2022-05-24 12:38:30', 1);
INSERT INTO `admin_role` VALUES (2, 2, 2, NULL, NULL, NULL, NULL, '2021-06-01 08:53:12', '2021-12-01 06:21:40', 1);
INSERT INTO `admin_role` VALUES (3, 3, 3, NULL, NULL, NULL, NULL, '2021-06-18 17:18:37', '2022-04-06 09:59:22', 1);
INSERT INTO `admin_role` VALUES (4, 4, 4, NULL, NULL, NULL, NULL, '2021-09-27 09:37:45', '2021-09-27 09:37:45', 0);
INSERT INTO `admin_role` VALUES (5, 4, 2, NULL, NULL, NULL, NULL, '2021-12-01 06:21:40', '2022-05-20 16:11:33', 1);
INSERT INTO `admin_role` VALUES (6, 1, 3, NULL, NULL, NULL, NULL, '2022-04-06 09:59:22', '2022-04-06 09:59:47', 1);
INSERT INTO `admin_role` VALUES (7, 2, 3, NULL, NULL, NULL, NULL, '2022-04-06 09:59:22', '2022-04-06 09:59:47', 1);
INSERT INTO `admin_role` VALUES (8, 1, 3, NULL, NULL, NULL, NULL, '2022-04-06 09:59:47', '2022-04-06 09:59:54', 1);
INSERT INTO `admin_role` VALUES (9, 1, 3, NULL, NULL, NULL, NULL, '2022-04-06 09:59:54', '2022-04-06 11:25:34', 1);
INSERT INTO `admin_role` VALUES (10, 2, 3, NULL, NULL, NULL, NULL, '2022-04-06 09:59:54', '2022-04-06 11:25:34', 1);
INSERT INTO `admin_role` VALUES (11, 2, 3, NULL, NULL, NULL, NULL, '2022-04-06 11:25:34', '2022-06-23 17:05:50', 1);
INSERT INTO `admin_role` VALUES (12, 2, 2, NULL, NULL, NULL, NULL, '2022-05-20 16:11:34', '2022-05-20 16:11:34', 0);
INSERT INTO `admin_role` VALUES (13, 1, 1, NULL, NULL, NULL, NULL, '2022-05-24 12:38:30', '2022-05-24 12:38:30', 0);
INSERT INTO `admin_role` VALUES (14, 2, 3, NULL, NULL, NULL, NULL, '2022-06-23 17:05:50', '2022-06-23 17:05:50', 0);
COMMIT;

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `pid` bigint(20) NOT NULL DEFAULT '0' COMMENT '所属上级',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '名称',
  `code` varchar(50) DEFAULT NULL COMMENT '名称code',
  `to_code` varchar(100) DEFAULT NULL COMMENT '跳转页面code',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型(1:菜单,2:按钮)',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态(0:禁止,1:正常)',
  `create_id` bigint(20) DEFAULT NULL,
  `create_name` varchar(20) DEFAULT NULL,
  `update_id` bigint(20) DEFAULT NULL,
  `update_name` varchar(20) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标记（0:不可用 1:可用）',
  PRIMARY KEY (`id`),
  KEY `idx_pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=utf8mb4 COMMENT='权限';

-- ----------------------------
-- Records of permission
-- ----------------------------
BEGIN;
INSERT INTO `permission` VALUES (1, 0, '全部数据', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-09-27 13:37:59', 0);
INSERT INTO `permission` VALUES (2, 1, '权限管理', 'Acl', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-05-31 19:36:53', 0);
INSERT INTO `permission` VALUES (3, 2, '用户管理', 'User', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-05-31 19:36:58', 0);
INSERT INTO `permission` VALUES (4, 2, '角色管理', 'Role', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-05-31 19:37:02', 0);
INSERT INTO `permission` VALUES (5, 2, '菜单管理', 'Permission', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-05-31 19:37:05', 0);
INSERT INTO `permission` VALUES (6, 3, '分配角色', 'btn.User.assgin', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-06-01 08:35:35', 0);
INSERT INTO `permission` VALUES (7, 3, '添加', 'btn.User.add', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-06-01 08:34:29', 0);
INSERT INTO `permission` VALUES (8, 3, '修改', 'btn.User.update', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-06-01 08:34:45', 0);
INSERT INTO `permission` VALUES (9, 3, '删除', 'btn.User.remove', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-06-01 08:34:38', 0);
INSERT INTO `permission` VALUES (10, 4, '修改', 'btn.Role.update', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-06-01 08:36:20', 0);
INSERT INTO `permission` VALUES (11, 4, '分配权限', 'btn.Role.assgin', 'RoleAuth', 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-06-01 08:36:56', 0);
INSERT INTO `permission` VALUES (12, 4, '添加', 'btn.Role.add', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-06-01 08:36:08', 0);
INSERT INTO `permission` VALUES (13, 4, '删除', 'btn.Role.remove', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-06-01 08:36:32', 0);
INSERT INTO `permission` VALUES (14, 4, '角色权限', 'role.acl', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-06-01 08:37:22', 1);
INSERT INTO `permission` VALUES (15, 5, '查看', 'btn.permission.list', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-05-31 19:32:52', 0);
INSERT INTO `permission` VALUES (16, 5, '添加', 'btn.Permission.add', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-06-01 08:37:39', 0);
INSERT INTO `permission` VALUES (17, 5, '修改', 'btn.Permission.update', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-06-01 08:37:47', 0);
INSERT INTO `permission` VALUES (18, 5, '删除', 'btn.Permission.remove', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:05:37', '2021-06-01 08:37:54', 0);
INSERT INTO `permission` VALUES (19, 1, '数据接入', 'dataaccess', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2021-06-18 16:38:51', '2022-04-20 11:00:09', 0);
INSERT INTO `permission` VALUES (20, 19, 'mysql', 'mysql', '', 1, NULL, NULL, NULL, NULL, NULL, '2021-06-18 16:39:21', '2022-04-20 11:01:03', 0);
INSERT INTO `permission` VALUES (21, 1, '统计管理', 'statistics', NULL, 1, NULL, NULL, NULL, 1, 'admin', '2022-03-17 07:55:29', '2022-03-17 07:56:13', 0);
INSERT INTO `permission` VALUES (100, 1, '', 'btn.all', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-03-22 14:32:59', '2022-04-07 09:00:26', 1);
INSERT INTO `permission` VALUES (102, 19, 'postgreSQL', 'postgreSQL', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-03-17 02:16:26', '2022-04-20 11:01:19', 0);
INSERT INTO `permission` VALUES (103, 19, 'mongoDB', 'mongoDB', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-03-17 02:16:47', '2022-04-20 11:01:58', 0);
INSERT INTO `permission` VALUES (104, 21, '实时统计', 'real', NULL, 1, NULL, NULL, NULL, 1, 'admin', '2022-03-17 03:24:01', '2022-03-17 07:55:37', 0);
INSERT INTO `permission` VALUES (105, 21, 'sugar', 'sugar', NULL, 1, NULL, NULL, NULL, 1, 'admin', '2022-03-17 03:24:09', '2022-03-17 07:55:38', 0);
INSERT INTO `permission` VALUES (106, 21, '访问流量统计', 'visit', NULL, 1, NULL, NULL, NULL, 1, 'admin', '2022-03-17 03:24:20', '2022-03-17 07:55:39', 0);
INSERT INTO `permission` VALUES (107, 21, '会员统计', 'user', NULL, 1, NULL, NULL, NULL, 1, 'admin', '2022-03-17 03:24:26', '2022-03-17 07:55:39', 0);
INSERT INTO `permission` VALUES (108, 1, '报表管理', 'report', '', 1, NULL, NULL, NULL, 1, 'admin', '2022-03-18 02:17:20', '2022-03-18 02:17:20', 0);
INSERT INTO `permission` VALUES (109, 108, '报表管理', 'customQuery', '', 1, NULL, NULL, NULL, 1, 'admin', '2022-03-18 02:18:13', '2022-03-18 02:18:13', 0);
INSERT INTO `permission` VALUES (110, 108, '入库审核', 'InApproveTask', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-03-18 08:36:14', '2022-04-20 11:13:24', 1);
INSERT INTO `permission` VALUES (111, 108, '收货任务', 'InReceivingTask', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2022-03-21 11:49:09', '2022-04-20 11:13:12', 1);
INSERT INTO `permission` VALUES (112, 108, '上架任务', 'InPutawayTask', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2022-03-21 15:58:48', '2022-04-20 11:13:02', 1);
INSERT INTO `permission` VALUES (113, 109, '入库单详情', 'btn.InOrder.show', 'InOrderShow', 2, NULL, NULL, NULL, NULL, NULL, '2022-03-22 09:53:45', '2022-04-20 11:13:49', 1);
INSERT INTO `permission` VALUES (114, 1, '任务调度', 'schedule', '', 1, NULL, NULL, NULL, 1, 'admin', '2022-03-22 14:33:39', '2022-03-22 14:33:39', 0);
INSERT INTO `permission` VALUES (115, 114, 'dolphinScheduler调度', 'dolphinScheduler', '', 1, NULL, NULL, NULL, 1, 'admin', '2022-03-22 14:34:24', '2022-03-22 14:34:24', 0);
INSERT INTO `permission` VALUES (116, 114, '调度', 'scheduleList', '', 1, NULL, NULL, NULL, 1, 'admin', '2022-03-23 09:48:40', '2022-03-23 09:48:40', 0);
INSERT INTO `permission` VALUES (117, 114, '盘点任务', 'InvCountingTask', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2022-03-23 15:52:20', '2022-04-20 11:11:21', 1);
INSERT INTO `permission` VALUES (118, 115, '盘点单详情', 'btn.InvCounting.show', 'InvCountingShow', 2, NULL, NULL, NULL, NULL, NULL, '2022-03-24 10:04:31', '2022-04-20 11:14:31', 1);
INSERT INTO `permission` VALUES (119, 1, '出库管理', 'Outbound', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-03-25 08:53:23', '2022-04-20 11:12:45', 1);
INSERT INTO `permission` VALUES (120, 119, '出库单管理', 'OutOrder', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-03-25 08:53:57', '2022-04-20 11:12:42', 1);
INSERT INTO `permission` VALUES (121, 120, '出库单详情', 'bnt.OutOrderShow', 'OutOrderShow', 2, NULL, NULL, NULL, NULL, NULL, '2022-03-25 09:42:25', '2022-04-20 11:12:40', 1);
INSERT INTO `permission` VALUES (122, 119, '拣货任务', 'OutPickingTask', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-03-29 15:45:41', '2022-04-20 11:12:33', 1);
INSERT INTO `permission` VALUES (123, 119, '发货任务', 'OutDeliverTask', NULL, 1, NULL, NULL, NULL, NULL, NULL, '2022-03-30 10:45:59', '2022-04-20 11:12:23', 1);
INSERT INTO `permission` VALUES (124, 20, '查看', 'btn.mysql.list', '', 2, NULL, NULL, NULL, 1, 'admin', '2022-04-07 09:21:11', '2022-04-07 09:21:11', 0);
INSERT INTO `permission` VALUES (125, 102, '查看', 'btn.postgreSQL.list', NULL, 2, NULL, NULL, NULL, 1, 'admin', '2022-04-07 09:25:22', '2022-04-07 09:25:22', 0);
INSERT INTO `permission` VALUES (126, 102, '添加', 'btn.shipperInfo.add', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:25:22', '2022-04-20 11:17:15', 1);
INSERT INTO `permission` VALUES (127, 102, '修改', 'btn.shipperInfo.update', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:25:22', '2022-04-20 11:17:17', 1);
INSERT INTO `permission` VALUES (128, 102, '删除', 'btn.shipperInfo.remove', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:25:22', '2022-04-20 11:17:19', 1);
INSERT INTO `permission` VALUES (129, 103, '查看', 'btn.mongoDB.list', NULL, 2, NULL, NULL, NULL, 1, 'admin', '2022-04-07 09:26:13', '2022-04-07 09:26:13', 0);
INSERT INTO `permission` VALUES (130, 103, '添加', 'btn.goodsInfo.add', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:26:13', '2022-04-20 11:17:29', 1);
INSERT INTO `permission` VALUES (131, 103, '修改', 'btn.goodsInfo.update', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:26:13', '2022-04-20 11:17:26', 1);
INSERT INTO `permission` VALUES (132, 103, '删除', 'btn.goodsInfo.remove', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:26:13', '2022-04-20 11:17:24', 1);
INSERT INTO `permission` VALUES (133, 104, '查看', 'btn.real.list', NULL, 2, NULL, NULL, NULL, 1, 'admin', '2022-04-07 09:26:50', '2022-04-07 09:26:50', 0);
INSERT INTO `permission` VALUES (134, 104, '添加', 'btn.warehouseInfo.add', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:26:50', '2022-04-20 11:14:55', 1);
INSERT INTO `permission` VALUES (135, 104, '修改', 'btn.warehouseInfo.update', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:26:50', '2022-04-20 11:14:57', 1);
INSERT INTO `permission` VALUES (136, 104, '删除', 'btn.warehouseInfo.remove', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:26:50', '2022-04-20 11:14:59', 1);
INSERT INTO `permission` VALUES (137, 105, '查看', 'btn.sugar.list', NULL, 2, NULL, NULL, NULL, 1, 'admin', '2022-04-07 09:27:19', '2022-04-07 09:27:19', 0);
INSERT INTO `permission` VALUES (138, 105, '添加', 'btn.storeareaInfo.add', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:27:19', '2022-04-20 11:15:08', 1);
INSERT INTO `permission` VALUES (139, 105, '修改', 'btn.storeareaInfo.update', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:27:19', '2022-04-20 11:15:11', 1);
INSERT INTO `permission` VALUES (140, 105, '删除', 'btn.storeareaInfo.remove', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:27:19', '2022-04-20 11:15:14', 1);
INSERT INTO `permission` VALUES (141, 106, '查看', 'btn.visit.list', NULL, 2, NULL, NULL, NULL, 1, 'admin', '2022-04-07 09:27:34', '2022-04-07 09:27:34', 0);
INSERT INTO `permission` VALUES (142, 106, '添加', 'btn.storeshelfInfo.add', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:27:34', '2022-04-20 11:15:25', 1);
INSERT INTO `permission` VALUES (143, 106, '修改', 'btn.storeshelfInfo.update', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:27:34', '2022-04-20 11:15:27', 1);
INSERT INTO `permission` VALUES (144, 106, '删除', 'btn.storeshelfInfo.remove', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:27:34', '2022-04-20 11:15:30', 1);
INSERT INTO `permission` VALUES (145, 107, '查看', 'btn.user.list', NULL, 2, NULL, NULL, NULL, 1, 'admin', '2022-04-07 09:27:54', '2022-04-07 09:27:54', 0);
INSERT INTO `permission` VALUES (146, 107, '添加', 'btn.storehouseInfo.add', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:27:54', '2022-04-20 11:15:39', 1);
INSERT INTO `permission` VALUES (147, 107, '修改', 'btn.storehouseInfo.update', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:27:54', '2022-04-20 11:15:41', 1);
INSERT INTO `permission` VALUES (148, 107, '删除', 'btn.storehouseInfo.remove', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:27:54', '2022-04-20 11:15:43', 1);
INSERT INTO `permission` VALUES (149, 109, '查看', 'btn.customQuery.list', NULL, 2, NULL, NULL, NULL, 1, 'admin', '2022-04-07 09:32:36', '2022-04-07 09:32:36', 0);
INSERT INTO `permission` VALUES (150, 109, '添加', 'btn.inOrder.add', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:32:36', '2022-04-20 11:13:45', 1);
INSERT INTO `permission` VALUES (151, 109, '修改', 'btn.inOrder.update', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:32:36', '2022-04-20 11:13:43', 1);
INSERT INTO `permission` VALUES (152, 109, '删除', 'btn.inOrder.remove', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:32:36', '2022-04-20 11:13:40', 1);
INSERT INTO `permission` VALUES (153, 109, '提交审批', 'btn.inOrder.approve', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:32:36', '2022-04-20 11:13:31', 1);
INSERT INTO `permission` VALUES (155, 110, '查看', 'btn.inApproveTask.list', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:35:12', '2022-04-20 11:13:21', 1);
INSERT INTO `permission` VALUES (156, 110, '审批', 'btn.inApproveTask.approve', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:35:12', '2022-04-20 11:13:18', 1);
INSERT INTO `permission` VALUES (157, 111, '查看', 'btn.inReceivingTask.list', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:36:05', '2022-04-20 11:13:09', 1);
INSERT INTO `permission` VALUES (158, 111, '收货', 'btn.inReceivingTask.receiving', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:36:05', '2022-04-20 11:13:07', 1);
INSERT INTO `permission` VALUES (159, 112, '查看', 'btn.inPutawayTask.list', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:37:20', '2022-04-20 11:13:00', 1);
INSERT INTO `permission` VALUES (160, 112, '上架', 'btn.inPutawayTask.putaway', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:37:20', '2022-04-20 11:12:57', 1);
INSERT INTO `permission` VALUES (161, 112, '同步库存', 'btn.inPutawayTask.sync', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:37:20', '2022-04-20 11:12:55', 1);
INSERT INTO `permission` VALUES (162, 116, '查看', 'btn.scheduleList.list', NULL, 2, NULL, NULL, NULL, 1, 'admin', '2022-04-07 09:38:22', '2022-04-07 09:38:22', 0);
INSERT INTO `permission` VALUES (163, 115, '查看', 'btn.dolphinScheduler.list', NULL, 2, NULL, NULL, NULL, 1, 'admin', '2022-04-07 09:39:57', '2022-04-07 09:39:57', 0);
INSERT INTO `permission` VALUES (164, 115, '添加', 'btn.invCounting.add', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:39:57', '2022-04-20 11:14:24', 1);
INSERT INTO `permission` VALUES (165, 115, '修改', 'btn.invCounting.update', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:39:57', '2022-04-20 11:14:22', 1);
INSERT INTO `permission` VALUES (166, 115, '删除', 'btn.invCounting.remove', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:39:57', '2022-04-20 11:14:27', 1);
INSERT INTO `permission` VALUES (167, 115, '分配任务', 'btn.invCounting.assign', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:39:57', '2022-04-20 11:14:20', 1);
INSERT INTO `permission` VALUES (168, 117, '查看', 'btn.invCountingTask.list', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:41:40', '2022-04-20 11:11:11', 1);
INSERT INTO `permission` VALUES (169, 117, '更新数据', 'btn.invCountingTask.update', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:41:40', '2022-04-20 11:11:15', 1);
INSERT INTO `permission` VALUES (170, 117, '同步库存', 'btn.invCountingTask.sync', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:41:40', '2022-04-20 11:11:18', 1);
INSERT INTO `permission` VALUES (171, 120, '查看', 'btn.outOrder.list', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:42:54', '2022-04-20 11:12:38', 1);
INSERT INTO `permission` VALUES (172, 122, '查看', 'btn.outPickingTask.list', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:43:57', '2022-04-20 11:12:31', 1);
INSERT INTO `permission` VALUES (173, 122, '完成拣货', 'btn.outPickingTask.finish', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:43:57', '2022-04-20 11:12:28', 1);
INSERT INTO `permission` VALUES (174, 123, '查看', 'btn.outDeliverTask.list', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:44:43', '2022-04-20 11:12:21', 1);
INSERT INTO `permission` VALUES (175, 123, '发货', 'btn.outDeliverTask.deliver', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:44:43', '2022-04-20 11:12:18', 1);
INSERT INTO `permission` VALUES (176, 3, '分配仓库', 'btn.User.assginWarehouse', NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 09:51:53', '2022-04-07 09:52:05', 0);
INSERT INTO `permission` VALUES (177, 1, '配置管理', 'Config', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-04-07 11:07:22', '2022-04-20 11:12:11', 1);
INSERT INTO `permission` VALUES (178, 177, '配置列表', 'WareConfig', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-04-07 11:07:47', '2022-04-20 11:12:08', 1);
INSERT INTO `permission` VALUES (179, 178, '查看', 'bnt.wareConfig.list', '', 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 11:08:30', '2022-04-20 11:12:08', 1);
INSERT INTO `permission` VALUES (180, 178, '更新', 'bnt.wareConfig.update', '', 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 11:08:48', '2022-04-20 11:12:05', 1);
INSERT INTO `permission` VALUES (181, 116, '手动上架', 'btn.inventoryInfo.updateInventory', '', 2, NULL, NULL, NULL, NULL, NULL, '2022-04-07 15:39:06', '2022-04-20 11:14:08', 1);
INSERT INTO `permission` VALUES (182, 19, 'sqlServer', 'sqlServer', '', 1, NULL, 1, 'admin', NULL, NULL, '2022-04-08 03:42:01', '2022-04-20 11:01:49', 0);
INSERT INTO `permission` VALUES (183, 182, '查看', 'bnt.sqlServer.list', '', 2, NULL, 1, 'admin', 1, 'admin', '2022-04-08 03:42:19', '2022-04-08 03:42:19', 0);
INSERT INTO `permission` VALUES (184, 114, '库内转移', 'InvMove', '', 1, NULL, 1, 'admin', NULL, NULL, '2022-04-12 01:45:25', '2022-04-20 11:11:37', 1);
INSERT INTO `permission` VALUES (185, 184, '转移单详情', 'btn.InvMoveShow.show', 'InvMoveShow', 2, NULL, 1, 'admin', NULL, NULL, '2022-04-12 01:46:02', '2022-04-20 11:11:37', 1);
INSERT INTO `permission` VALUES (186, 114, '转移任务', 'InvMoveTask', '', 1, NULL, 1, 'admin', NULL, NULL, '2022-04-12 01:46:23', '2022-04-20 11:11:56', 1);
INSERT INTO `permission` VALUES (187, 186, '查看', 'btn.InvMoveTask.list', '', 2, NULL, 1, 'admin', NULL, NULL, '2022-04-12 01:46:55', '2022-04-20 11:11:53', 1);
INSERT INTO `permission` VALUES (188, 184, '添加', 'btn.invMove.add', '', 2, NULL, 1, 'admin', NULL, NULL, '2022-04-12 01:57:51', '2022-04-20 11:11:35', 1);
INSERT INTO `permission` VALUES (189, 184, '修改', 'btn.invMove.update', '', 2, NULL, 1, 'admin', NULL, NULL, '2022-04-12 01:58:02', '2022-04-20 11:11:37', 1);
INSERT INTO `permission` VALUES (190, 184, '删除', 'btn.invMove.remove', '', 2, NULL, 1, 'admin', NULL, NULL, '2022-04-12 01:58:19', '2022-04-20 11:11:32', 1);
INSERT INTO `permission` VALUES (191, 184, '分配', 'btn.invMove.assign', '', 2, NULL, 1, 'admin', NULL, NULL, '2022-04-12 13:56:34', '2022-04-20 11:11:29', 1);
INSERT INTO `permission` VALUES (192, 186, '完成', 'btn.invMoveTask.update', '', 2, NULL, 1, 'admin', NULL, NULL, '2022-04-12 14:00:47', '2022-04-20 11:11:50', 1);
INSERT INTO `permission` VALUES (193, 186, '同步库存', 'btn.invMoveTask.sync', '', 2, NULL, 1, 'admin', NULL, NULL, '2022-04-12 15:10:11', '2022-04-20 11:11:48', 1);
INSERT INTO `permission` VALUES (194, 21, '商品分析', 'goods', '', 1, NULL, 1, 'admin', NULL, NULL, '2022-04-20 11:06:54', '2022-04-20 11:06:54', 0);
INSERT INTO `permission` VALUES (195, 21, '订单统计', 'order', '', 1, NULL, 1, 'admin', NULL, NULL, '2022-04-20 11:07:07', '2022-04-20 11:07:07', 0);
INSERT INTO `permission` VALUES (196, 21, '活动分析', 'activityTable', '', 1, NULL, 1, 'admin', NULL, NULL, '2022-04-20 11:07:20', '2022-04-20 11:07:20', 0);
INSERT INTO `permission` VALUES (197, 21, '优惠券分析', 'couponTable', '', 1, NULL, 1, 'admin', NULL, NULL, '2022-04-20 11:07:32', '2022-04-20 11:07:32', 0);
INSERT INTO `permission` VALUES (198, 194, '查看', 'btn.goods.list', '', 2, NULL, 1, 'admin', 1, 'admin', '2022-04-20 11:15:56', '2022-04-20 11:15:56', 0);
INSERT INTO `permission` VALUES (199, 195, '查看', 'bnt.order.list', '', 2, NULL, 1, 'admin', NULL, NULL, '2022-04-20 11:16:35', '2022-04-20 11:16:35', 0);
INSERT INTO `permission` VALUES (200, 196, '查看', 'bnt.activityTable.list', '', 2, NULL, 1, 'admin', NULL, NULL, '2022-04-20 11:16:50', '2022-04-20 11:16:50', 0);
INSERT INTO `permission` VALUES (201, 197, '查看', 'bnt.couponTable.list', '', 2, NULL, 1, 'admin', NULL, NULL, '2022-04-20 11:17:01', '2022-04-20 11:17:01', 0);
INSERT INTO `permission` VALUES (202, 1, '数据治理', 'govern', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-05-24 12:33:58', '2022-05-24 12:33:58', 0);
INSERT INTO `permission` VALUES (203, 202, '数据治理', 'governList', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-05-24 12:34:47', '2022-05-24 12:34:47', 0);
INSERT INTO `permission` VALUES (204, 202, '数据监控', 'tableList', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-05-24 12:35:03', '2022-07-25 09:18:58', 1);
INSERT INTO `permission` VALUES (205, 202, '数据详情', 'detail', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-05-24 12:35:34', '2022-07-25 09:21:16', 1);
INSERT INTO `permission` VALUES (206, 203, '查看', 'btn.governList.list', '', 2, NULL, NULL, NULL, NULL, NULL, '2022-05-24 12:36:19', '2022-05-24 12:36:19', 0);
INSERT INTO `permission` VALUES (207, 204, '查看', 'btn.tableList.list', '', 2, NULL, NULL, NULL, NULL, NULL, '2022-05-24 12:36:58', '2022-07-25 09:18:55', 1);
INSERT INTO `permission` VALUES (208, 205, '查看', 'btn.detail.list', '', 2, NULL, NULL, NULL, NULL, NULL, '2022-05-24 12:37:10', '2022-07-25 09:21:16', 1);
INSERT INTO `permission` VALUES (209, 202, '监控管理', 'monitorList', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-05-24 18:57:32', '2022-07-25 09:23:32', 1);
INSERT INTO `permission` VALUES (210, 209, '查看', 'btn.monitorList.list', '', 2, NULL, NULL, NULL, NULL, NULL, '2022-05-24 18:58:21', '2022-07-25 09:23:32', 1);
INSERT INTO `permission` VALUES (211, 1, 'jiuzhang', 'acl', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-07-12 10:31:56', '2022-07-12 10:40:27', 1);
INSERT INTO `permission` VALUES (212, 1, 'jiuzhang', 'acl', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-07-12 10:32:14', '2022-07-12 10:33:36', 1);
INSERT INTO `permission` VALUES (213, 211, '查看1111', 'btn.monitorList.list', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-07-12 10:33:27', '2022-07-12 10:38:49', 1);
INSERT INTO `permission` VALUES (214, 1, '数据资产', 'assets', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-07-25 09:17:25', '2022-07-25 09:17:25', 0);
INSERT INTO `permission` VALUES (215, 214, '数据资产', 'tableList', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-07-25 09:17:50', '2022-07-25 09:17:50', 0);
INSERT INTO `permission` VALUES (216, 215, '查看', 'btn.tableList.list', '', 2, NULL, NULL, NULL, NULL, NULL, '2022-07-25 09:18:28', '2022-07-25 09:18:28', 0);
INSERT INTO `permission` VALUES (217, 214, '数据详情', 'detail', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-07-25 09:20:51', '2022-07-25 09:20:51', 0);
INSERT INTO `permission` VALUES (218, 217, '查看', 'btn.detail.list', '', 2, NULL, NULL, NULL, NULL, NULL, '2022-07-25 09:21:04', '2022-07-25 09:21:04', 0);
INSERT INTO `permission` VALUES (219, 1, '数据监控', 'monitor', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-07-25 09:22:44', '2022-07-25 09:22:44', 0);
INSERT INTO `permission` VALUES (220, 219, '监控管理', 'monitorList', '', 1, NULL, NULL, NULL, NULL, NULL, '2022-07-25 09:23:12', '2022-07-25 09:23:12', 0);
INSERT INTO `permission` VALUES (221, 220, '查看', 'btn.monitorList.list', '', 2, NULL, NULL, NULL, NULL, NULL, '2022-07-25 09:23:29', '2022-07-25 09:23:29', 0);
COMMIT;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(20) NOT NULL DEFAULT '' COMMENT '角色名称',
  `role_code` varchar(20) DEFAULT NULL COMMENT '角色编码',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_id` bigint(20) DEFAULT NULL,
  `create_name` varchar(20) DEFAULT NULL,
  `update_id` bigint(20) DEFAULT NULL,
  `update_name` varchar(20) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标记（0:不可用 1:可用）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of role
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES (1, '系统管理员', 'SYSTEM', NULL, NULL, NULL, NULL, NULL, '2021-05-31 18:09:18', '2022-07-12 10:09:29', 0);
INSERT INTO `role` VALUES (2, '仓库管理员', NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-06 09:57:29', '2022-04-06 09:57:29', 0);
INSERT INTO `role` VALUES (3, '111', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-12 10:09:57', '2022-07-12 10:12:21', 1);
INSERT INTO `role` VALUES (4, '2222', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-12 10:12:36', '2022-07-12 10:13:03', 1);
INSERT INTO `role` VALUES (5, '3333', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-12 10:12:40', '2022-07-12 10:13:03', 1);
COMMIT;

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL DEFAULT '0',
  `permission_id` bigint(20) NOT NULL DEFAULT '0',
  `create_id` bigint(20) DEFAULT NULL,
  `create_name` varchar(20) DEFAULT NULL,
  `update_id` bigint(20) DEFAULT NULL,
  `update_name` varchar(20) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标记（0:不可用 1:可用）',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_permission_id` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=398 DEFAULT CHARSET=utf8 COMMENT='角色权限';

-- ----------------------------
-- Records of role_permission
-- ----------------------------
BEGIN;
INSERT INTO `role_permission` VALUES (102, 2, 1, NULL, NULL, NULL, NULL, '2022-05-20 17:05:10', '2022-05-24 12:39:41', 1);
INSERT INTO `role_permission` VALUES (103, 2, 2, NULL, NULL, NULL, NULL, '2022-05-20 17:05:10', '2022-05-24 12:39:41', 1);
INSERT INTO `role_permission` VALUES (104, 2, 3, NULL, NULL, NULL, NULL, '2022-05-20 17:05:10', '2022-05-24 12:39:41', 1);
INSERT INTO `role_permission` VALUES (105, 2, 9, NULL, NULL, NULL, NULL, '2022-05-20 17:05:10', '2022-05-24 12:39:41', 1);
INSERT INTO `role_permission` VALUES (106, 1, 1, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (107, 1, 2, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (108, 1, 3, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (109, 1, 6, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (110, 1, 7, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (111, 1, 8, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (112, 1, 9, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (113, 1, 176, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (114, 1, 4, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (115, 1, 10, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (116, 1, 11, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (117, 1, 12, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (118, 1, 13, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (119, 1, 5, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (120, 1, 15, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (121, 1, 16, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (122, 1, 17, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (123, 1, 18, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (124, 1, 19, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (125, 1, 20, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (126, 1, 124, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (127, 1, 102, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (128, 1, 125, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (129, 1, 103, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (130, 1, 129, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (131, 1, 182, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (132, 1, 183, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (133, 1, 21, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (134, 1, 104, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (135, 1, 133, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (136, 1, 105, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (137, 1, 137, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (138, 1, 106, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (139, 1, 141, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (140, 1, 107, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (141, 1, 145, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (142, 1, 194, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (143, 1, 198, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (144, 1, 195, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (145, 1, 199, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (146, 1, 196, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (147, 1, 200, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (148, 1, 197, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (149, 1, 201, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (150, 1, 108, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (151, 1, 109, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (152, 1, 149, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (153, 1, 114, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (154, 1, 115, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (155, 1, 163, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (156, 1, 116, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (157, 1, 162, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (158, 1, 202, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (159, 1, 203, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (160, 1, 206, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (161, 1, 204, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (162, 1, 207, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (163, 1, 205, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (164, 1, 208, NULL, NULL, NULL, NULL, '2022-05-24 12:37:40', '2022-05-24 12:41:17', 1);
INSERT INTO `role_permission` VALUES (165, 2, 1, NULL, NULL, NULL, NULL, '2022-05-24 12:39:41', '2022-07-29 22:43:23', 1);
INSERT INTO `role_permission` VALUES (166, 2, 2, NULL, NULL, NULL, NULL, '2022-05-24 12:39:41', '2022-07-29 22:43:23', 1);
INSERT INTO `role_permission` VALUES (167, 2, 3, NULL, NULL, NULL, NULL, '2022-05-24 12:39:41', '2022-07-29 22:43:23', 1);
INSERT INTO `role_permission` VALUES (168, 2, 9, NULL, NULL, NULL, NULL, '2022-05-24 12:39:41', '2022-07-29 22:43:23', 1);
INSERT INTO `role_permission` VALUES (169, 2, 202, NULL, NULL, NULL, NULL, '2022-05-24 12:39:41', '2022-07-29 22:43:23', 1);
INSERT INTO `role_permission` VALUES (170, 2, 203, NULL, NULL, NULL, NULL, '2022-05-24 12:39:41', '2022-07-29 22:43:23', 1);
INSERT INTO `role_permission` VALUES (171, 2, 206, NULL, NULL, NULL, NULL, '2022-05-24 12:39:41', '2022-07-29 22:43:23', 1);
INSERT INTO `role_permission` VALUES (172, 2, 204, NULL, NULL, NULL, NULL, '2022-05-24 12:39:41', '2022-07-29 22:43:23', 1);
INSERT INTO `role_permission` VALUES (173, 2, 207, NULL, NULL, NULL, NULL, '2022-05-24 12:39:41', '2022-07-29 22:43:23', 1);
INSERT INTO `role_permission` VALUES (174, 2, 205, NULL, NULL, NULL, NULL, '2022-05-24 12:39:41', '2022-07-29 22:43:23', 1);
INSERT INTO `role_permission` VALUES (175, 2, 208, NULL, NULL, NULL, NULL, '2022-05-24 12:39:41', '2022-07-29 22:43:23', 1);
INSERT INTO `role_permission` VALUES (176, 1, 1, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (177, 1, 2, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (178, 1, 3, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (179, 1, 6, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (180, 1, 7, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (181, 1, 8, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (182, 1, 9, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (183, 1, 176, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (184, 1, 4, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (185, 1, 10, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (186, 1, 11, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (187, 1, 12, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (188, 1, 13, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (189, 1, 5, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (190, 1, 15, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (191, 1, 16, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (192, 1, 17, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (193, 1, 18, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (194, 1, 19, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (195, 1, 20, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (196, 1, 124, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (197, 1, 102, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (198, 1, 125, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (199, 1, 103, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (200, 1, 129, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (201, 1, 182, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (202, 1, 183, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (203, 1, 21, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (204, 1, 104, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (205, 1, 133, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (206, 1, 105, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (207, 1, 137, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (208, 1, 106, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (209, 1, 141, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (210, 1, 107, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (211, 1, 145, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (212, 1, 194, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (213, 1, 198, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (214, 1, 195, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (215, 1, 199, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (216, 1, 196, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (217, 1, 200, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (218, 1, 197, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (219, 1, 201, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (220, 1, 108, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (221, 1, 109, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (222, 1, 149, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (223, 1, 114, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (224, 1, 115, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (225, 1, 163, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (226, 1, 116, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (227, 1, 162, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (228, 1, 202, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (229, 1, 203, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (230, 1, 206, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (231, 1, 204, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (232, 1, 207, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (233, 1, 205, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (234, 1, 208, NULL, NULL, NULL, NULL, '2022-05-24 12:41:17', '2022-05-24 18:58:34', 1);
INSERT INTO `role_permission` VALUES (235, 1, 1, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (236, 1, 2, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (237, 1, 3, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (238, 1, 6, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (239, 1, 7, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (240, 1, 8, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (241, 1, 9, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (242, 1, 176, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (243, 1, 4, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (244, 1, 10, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (245, 1, 11, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (246, 1, 12, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (247, 1, 13, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (248, 1, 5, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (249, 1, 15, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (250, 1, 16, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (251, 1, 17, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (252, 1, 18, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (253, 1, 19, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (254, 1, 20, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (255, 1, 124, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (256, 1, 102, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (257, 1, 125, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (258, 1, 103, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (259, 1, 129, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (260, 1, 182, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (261, 1, 183, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (262, 1, 21, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (263, 1, 104, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (264, 1, 133, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (265, 1, 105, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (266, 1, 137, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (267, 1, 106, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (268, 1, 141, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (269, 1, 107, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (270, 1, 145, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (271, 1, 194, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (272, 1, 198, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (273, 1, 195, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (274, 1, 199, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (275, 1, 196, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (276, 1, 200, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (277, 1, 197, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (278, 1, 201, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (279, 1, 108, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (280, 1, 109, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (281, 1, 149, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (282, 1, 114, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (283, 1, 115, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (284, 1, 163, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (285, 1, 116, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (286, 1, 162, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (287, 1, 202, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (288, 1, 203, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (289, 1, 206, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (290, 1, 204, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (291, 1, 207, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (292, 1, 205, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (293, 1, 208, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (294, 1, 209, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (295, 1, 210, NULL, NULL, NULL, NULL, '2022-05-24 18:58:34', '2022-07-25 09:23:42', 1);
INSERT INTO `role_permission` VALUES (296, 3, 1, NULL, NULL, NULL, NULL, '2022-07-12 10:11:21', '2022-07-12 10:11:21', 0);
INSERT INTO `role_permission` VALUES (297, 3, 19, NULL, NULL, NULL, NULL, '2022-07-12 10:11:21', '2022-07-12 10:11:21', 0);
INSERT INTO `role_permission` VALUES (298, 3, 20, NULL, NULL, NULL, NULL, '2022-07-12 10:11:21', '2022-07-12 10:11:21', 0);
INSERT INTO `role_permission` VALUES (299, 3, 124, NULL, NULL, NULL, NULL, '2022-07-12 10:11:21', '2022-07-12 10:11:21', 0);
INSERT INTO `role_permission` VALUES (300, 3, 202, NULL, NULL, NULL, NULL, '2022-07-12 10:11:21', '2022-07-12 10:11:21', 0);
INSERT INTO `role_permission` VALUES (301, 3, 209, NULL, NULL, NULL, NULL, '2022-07-12 10:11:21', '2022-07-12 10:11:21', 0);
INSERT INTO `role_permission` VALUES (302, 3, 210, NULL, NULL, NULL, NULL, '2022-07-12 10:11:21', '2022-07-12 10:11:21', 0);
INSERT INTO `role_permission` VALUES (303, 1, 1, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (304, 1, 2, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (305, 1, 3, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (306, 1, 6, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (307, 1, 7, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (308, 1, 8, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (309, 1, 9, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (310, 1, 176, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (311, 1, 4, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (312, 1, 10, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (313, 1, 11, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (314, 1, 12, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (315, 1, 13, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (316, 1, 5, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (317, 1, 15, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (318, 1, 16, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (319, 1, 17, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (320, 1, 18, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (321, 1, 19, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (322, 1, 20, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (323, 1, 124, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (324, 1, 102, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (325, 1, 125, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (326, 1, 103, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (327, 1, 129, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (328, 1, 182, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (329, 1, 183, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (330, 1, 21, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (331, 1, 104, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (332, 1, 133, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (333, 1, 105, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (334, 1, 137, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (335, 1, 106, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (336, 1, 141, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (337, 1, 107, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (338, 1, 145, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (339, 1, 194, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (340, 1, 198, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (341, 1, 195, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (342, 1, 199, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (343, 1, 196, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (344, 1, 200, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (345, 1, 197, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (346, 1, 201, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (347, 1, 108, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (348, 1, 109, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (349, 1, 149, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (350, 1, 114, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (351, 1, 115, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (352, 1, 163, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (353, 1, 116, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (354, 1, 162, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (355, 1, 202, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (356, 1, 203, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (357, 1, 206, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (358, 1, 214, NULL, NULL, NULL, NULL, '2022-07-25 09:23:42', '2022-07-25 09:23:42', 0);
INSERT INTO `role_permission` VALUES (359, 1, 215, NULL, NULL, NULL, NULL, '2022-07-25 09:23:43', '2022-07-25 09:23:43', 0);
INSERT INTO `role_permission` VALUES (360, 1, 216, NULL, NULL, NULL, NULL, '2022-07-25 09:23:43', '2022-07-25 09:23:43', 0);
INSERT INTO `role_permission` VALUES (361, 1, 217, NULL, NULL, NULL, NULL, '2022-07-25 09:23:43', '2022-07-25 09:23:43', 0);
INSERT INTO `role_permission` VALUES (362, 1, 218, NULL, NULL, NULL, NULL, '2022-07-25 09:23:43', '2022-07-25 09:23:43', 0);
INSERT INTO `role_permission` VALUES (363, 1, 219, NULL, NULL, NULL, NULL, '2022-07-25 09:23:43', '2022-07-25 09:23:43', 0);
INSERT INTO `role_permission` VALUES (364, 1, 220, NULL, NULL, NULL, NULL, '2022-07-25 09:23:43', '2022-07-25 09:23:43', 0);
INSERT INTO `role_permission` VALUES (365, 1, 221, NULL, NULL, NULL, NULL, '2022-07-25 09:23:43', '2022-07-25 09:23:43', 0);
INSERT INTO `role_permission` VALUES (366, 2, 1, NULL, NULL, NULL, NULL, '2022-07-29 22:43:23', '2022-07-29 22:51:03', 1);
INSERT INTO `role_permission` VALUES (367, 2, 2, NULL, NULL, NULL, NULL, '2022-07-29 22:43:23', '2022-07-29 22:51:03', 1);
INSERT INTO `role_permission` VALUES (368, 2, 3, NULL, NULL, NULL, NULL, '2022-07-29 22:43:23', '2022-07-29 22:51:03', 1);
INSERT INTO `role_permission` VALUES (369, 2, 8, NULL, NULL, NULL, NULL, '2022-07-29 22:43:23', '2022-07-29 22:51:03', 1);
INSERT INTO `role_permission` VALUES (370, 2, 9, NULL, NULL, NULL, NULL, '2022-07-29 22:43:23', '2022-07-29 22:51:03', 1);
INSERT INTO `role_permission` VALUES (371, 2, 202, NULL, NULL, NULL, NULL, '2022-07-29 22:43:23', '2022-07-29 22:51:03', 1);
INSERT INTO `role_permission` VALUES (372, 2, 203, NULL, NULL, NULL, NULL, '2022-07-29 22:43:23', '2022-07-29 22:51:03', 1);
INSERT INTO `role_permission` VALUES (373, 2, 206, NULL, NULL, NULL, NULL, '2022-07-29 22:43:23', '2022-07-29 22:51:03', 1);
INSERT INTO `role_permission` VALUES (374, 2, 1, NULL, NULL, NULL, NULL, '2022-07-29 22:51:03', '2022-07-29 23:18:22', 1);
INSERT INTO `role_permission` VALUES (375, 2, 2, NULL, NULL, NULL, NULL, '2022-07-29 22:51:03', '2022-07-29 23:18:22', 1);
INSERT INTO `role_permission` VALUES (376, 2, 3, NULL, NULL, NULL, NULL, '2022-07-29 22:51:03', '2022-07-29 23:18:22', 1);
INSERT INTO `role_permission` VALUES (377, 2, 8, NULL, NULL, NULL, NULL, '2022-07-29 22:51:03', '2022-07-29 23:18:22', 1);
INSERT INTO `role_permission` VALUES (378, 2, 9, NULL, NULL, NULL, NULL, '2022-07-29 22:51:03', '2022-07-29 23:18:22', 1);
INSERT INTO `role_permission` VALUES (379, 2, 202, NULL, NULL, NULL, NULL, '2022-07-29 22:51:03', '2022-07-29 23:18:22', 1);
INSERT INTO `role_permission` VALUES (380, 2, 203, NULL, NULL, NULL, NULL, '2022-07-29 22:51:03', '2022-07-29 23:18:22', 1);
INSERT INTO `role_permission` VALUES (381, 2, 206, NULL, NULL, NULL, NULL, '2022-07-29 22:51:03', '2022-07-29 23:18:22', 1);
INSERT INTO `role_permission` VALUES (382, 2, 1, NULL, NULL, NULL, NULL, '2022-07-29 23:18:22', '2022-07-30 00:04:16', 1);
INSERT INTO `role_permission` VALUES (383, 2, 2, NULL, NULL, NULL, NULL, '2022-07-29 23:18:22', '2022-07-30 00:04:16', 1);
INSERT INTO `role_permission` VALUES (384, 2, 3, NULL, NULL, NULL, NULL, '2022-07-29 23:18:22', '2022-07-30 00:04:16', 1);
INSERT INTO `role_permission` VALUES (385, 2, 8, NULL, NULL, NULL, NULL, '2022-07-29 23:18:22', '2022-07-30 00:04:16', 1);
INSERT INTO `role_permission` VALUES (386, 2, 9, NULL, NULL, NULL, NULL, '2022-07-29 23:18:22', '2022-07-30 00:04:16', 1);
INSERT INTO `role_permission` VALUES (387, 2, 202, NULL, NULL, NULL, NULL, '2022-07-29 23:18:22', '2022-07-30 00:04:16', 1);
INSERT INTO `role_permission` VALUES (388, 2, 203, NULL, NULL, NULL, NULL, '2022-07-29 23:18:22', '2022-07-30 00:04:16', 1);
INSERT INTO `role_permission` VALUES (389, 2, 206, NULL, NULL, NULL, NULL, '2022-07-29 23:18:22', '2022-07-30 00:04:16', 1);
INSERT INTO `role_permission` VALUES (390, 2, 1, NULL, NULL, NULL, NULL, '2022-07-30 00:04:16', '2022-07-30 00:04:16', 0);
INSERT INTO `role_permission` VALUES (391, 2, 2, NULL, NULL, NULL, NULL, '2022-07-30 00:04:16', '2022-07-30 00:04:16', 0);
INSERT INTO `role_permission` VALUES (392, 2, 3, NULL, NULL, NULL, NULL, '2022-07-30 00:04:16', '2022-07-30 00:04:16', 0);
INSERT INTO `role_permission` VALUES (393, 2, 8, NULL, NULL, NULL, NULL, '2022-07-30 00:04:16', '2022-07-30 00:04:16', 0);
INSERT INTO `role_permission` VALUES (394, 2, 9, NULL, NULL, NULL, NULL, '2022-07-30 00:04:16', '2022-07-30 00:04:16', 0);
INSERT INTO `role_permission` VALUES (395, 2, 202, NULL, NULL, NULL, NULL, '2022-07-30 00:04:16', '2022-07-30 00:04:16', 0);
INSERT INTO `role_permission` VALUES (396, 2, 203, NULL, NULL, NULL, NULL, '2022-07-30 00:04:16', '2022-07-30 00:04:16', 0);
INSERT INTO `role_permission` VALUES (397, 2, 206, NULL, NULL, NULL, NULL, '2022-07-30 00:04:16', '2022-07-30 00:04:16', 0);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
