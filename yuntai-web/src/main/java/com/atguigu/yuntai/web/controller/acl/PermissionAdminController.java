package com.atguigu.yuntai.web.controller.acl;

import com.atguigu.yuntai.acl.entity.Permission;
import com.atguigu.yuntai.acl.service.PermissionService;
import com.atguigu.yuntai.common.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 菜单管理 前端控制器
 * </p>
 */
@RestController
@RequestMapping("/admin/acl/permission")
@Api(tags = "菜单管理")
//@CrossOrigin //跨域
@Slf4j
public class PermissionAdminController {

    @Autowired
    private PermissionService permissionService;

    /**
     * 获取菜单
     *
     * @return
     */
    @ApiOperation(value = "获取菜单")
    @GetMapping
    public Result index() {
        List<Permission> list = permissionService.queryAllMenu();
        return Result.ok(list);
    }

    /**
     * 根据角色获取菜单
     *
     * @param roleId
     * @return
     */
    @ApiOperation(value = "根据角色获取菜单")
    @GetMapping("toAssign/{roleId}")
    public Result toAssign(@PathVariable Long roleId) {
        List<Permission> list = permissionService.selectAllMenu(roleId);
        return Result.ok(list);
    }

    /**
     * 给角色分配权限
     *
     * @param roleId
     * @param permissionId
     * @return
     */
    @ApiOperation(value = "给角色分配权限")
    @PostMapping("/doAssign")
    public Result doAssign(Long roleId, Long[] permissionId) {
        permissionService.saveRolePermissionRealtionShip(roleId, permissionId);
        return Result.ok();
    }

    /**
     * 新增菜单
     *
     * @param permission
     * @return
     */
    @ApiOperation(value = "新增菜单")
    @PostMapping("save")
    public Result save(@RequestBody Permission permission) {
        permissionService.save(permission);
        return Result.ok();
    }

    /**
     * 修改菜单
     *
     * @param permission
     * @return
     */
    @ApiOperation(value = "修改菜单")
    @PutMapping("update")
    public Result updateById(@RequestBody Permission permission) {
        permissionService.updateById(permission);
        return Result.ok();
    }

    /**
     * 递归删除菜单
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "递归删除菜单")
    @DeleteMapping("remove/{id}")
    public Result remove(@PathVariable Long id) {
        permissionService.removeChildById(id);
        return Result.ok();
    }

}

