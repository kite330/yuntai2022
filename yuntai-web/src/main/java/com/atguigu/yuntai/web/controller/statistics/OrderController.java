package com.atguigu.yuntai.web.controller.statistics;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.service.OrderProvinceService;
import com.atguigu.yuntai.statistics.service.TradeStatsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 交易主题
 */
@RestController
@RequestMapping("/statistics/order")
@Api(tags = "交易主题指标")
//@CrossOrigin //跨域
@Slf4j
public class OrderController {


    @Autowired
    private TradeStatsService tradeStatsService;

    @Autowired
    private OrderProvinceService orderProvinceService;


    /**
     * 订单统计
     *
     * @param qUnified
     * @return
     */
    @ApiOperation(value = "订单统计")
    @GetMapping("getTradeByDaysAndDt")
    @CrossOrigin
    public Result getTradeByDaysAndDt(
            @ApiParam(name = "qUnified", value = "查询条件", required = true)
                    QUnified qUnified) {
        return Result.ok(tradeStatsService.getTradeByDaysAndDt(qUnified));
    }


    /**
     * 各省份交易统计针对订单数
     *
     * @param qUnified
     * @return
     */
    @ApiOperation(value = " 各省份交易统计针对订单数")
    @GetMapping("getOrderNumProvinceData")
    @CrossOrigin
    public Result getOrderNumProvinceData(
            @ApiParam(name = "qUnified", value = "查询条件", required = true)
                    QUnified qUnified) {
        return Result.ok(orderProvinceService.getOrderNumProvinceData(qUnified));
    }

    /**
     * 各省份交易统计针对订单金额
     *
     * @param qUnified
     * @return
     */
    @ApiOperation(value = " 各省份交易统计针对订单金额")
    @GetMapping("getOrderMoneyProvinceData")
    @CrossOrigin
    public Result getOrderMoneyProvinceData(
            @ApiParam(name = "qUnified", value = "查询条件", required = true)
                    QUnified qUnified) {
        return Result.ok(orderProvinceService.getOrderMoneyProvinceData(qUnified));
    }
}
