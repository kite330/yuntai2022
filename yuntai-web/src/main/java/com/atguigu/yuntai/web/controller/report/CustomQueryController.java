package com.atguigu.yuntai.web.controller.report;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.report.bean.QCustomQuery;
import com.atguigu.yuntai.report.service.CustomQueryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 *  多数据源查询  前端控制器
 */
@RestController
@RequestMapping("/report")
@Api(tags = "多数据源查询")
//@CrossOrigin //跨域
@Slf4j
public class CustomQueryController {

    @Autowired
    private CustomQueryService customQueryService;


    /**
     * 自定义查询
     *
     * @param qCustomQuery
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "自定义查询")
    @PostMapping("querySql")
    public Result querySqlForType(@RequestBody QCustomQuery qCustomQuery) throws Exception {
        return Result.ok(customQueryService.querySqlForType(qCustomQuery));
    }
}
