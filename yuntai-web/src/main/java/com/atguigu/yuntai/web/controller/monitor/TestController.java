package com.atguigu.yuntai.web.controller.monitor;

import com.atguigu.yuntai.common.util.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    @PostMapping("/test")
    public Result test(@RequestBody String data){
      log.info(data);
      return Result.ok();
    }
}