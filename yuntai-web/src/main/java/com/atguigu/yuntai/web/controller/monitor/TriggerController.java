package com.atguigu.yuntai.web.controller.monitor;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.monitor.bean.TriggerLogRowVO;
import com.atguigu.yuntai.monitor.service.RuleInfoComputeService;
import com.atguigu.yuntai.monitor.service.TriggerLogService;
import com.atguigu.yuntai.monitor.service.TriggerService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 与触发相关的 前端控制器
 **/
@RestController
@RequestMapping("/monitor/trigger")
@Api(tags = "触发")
//@CrossOrigin //跨域
@Slf4j
public class TriggerController {

    @Autowired
    TriggerService triggerService;

    @Autowired
    TriggerLogService triggerLogService;

    @Autowired
    RuleInfoComputeService ruleInfoComputeService;

    /**
     * 查询触发记录
     * @param page
     * @param limit
     * @return
     */
    @ApiOperation( "查询触发记录")
    @GetMapping("getTriggerLogVO/{page}/{limit}")
    @CrossOrigin
    public Result getTriggerLogVO(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,
            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit) {
        Page<TriggerLogRowVO> pageParam = new Page<>(page, limit);
        return Result.ok(triggerLogService.listTriggerLogRowVO(pageParam));
    }

    /**
     * 触发一个集合的规则执行
     * @param who
     * @param collectionId
     * @return
     */
    @ApiOperation("触发一个集合的规则执行")
    @GetMapping( "/triggerExecute/{who}/{collectionId}")
    @CrossOrigin
    public Result trigger(
            @ApiParam(name = "who", value = "触发人", required = true)
            @PathVariable String who,

            @ApiParam(name = "collectionId", value = "规则集合ID", required = true)
            @PathVariable Long collectionId) {
        triggerService.triggerCollection(who, collectionId);
        return Result.ok();
    }

    /**
     * 根据触发id查看执行结果
     * @param triggerId
     * @return
     */
    @ApiOperation( "根据触发id查看执行结果")
    @GetMapping( "/getRuleInfoComputeVO/{triggerId}")
    public Result getRuleInfoComputeVO(
            @ApiParam(name = "triggerId", value = "触发Id", required = true)
            @PathVariable Long triggerId) {
        return Result.ok(ruleInfoComputeService.getRuleInfoComputeVOByTriggerId(triggerId));
    }
}
