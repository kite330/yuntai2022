package com.atguigu.yuntai.web.controller.monitor;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.monitor.bean.PartitionDef;
import com.atguigu.yuntai.monitor.service.HiveMetaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.hive.metastore.api.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Hive元数据 前端控制器
 */
@RestController
@RequestMapping(value = "/monitor/hiveMetaStore")
@Api(tags = "Hive元数据")
//@CrossOrigin //跨域
@Slf4j
public class HiveMetaStoreController {

    @Autowired
    HiveMetaService hiveMetaService;

    /**
     * 列出hive上全部的数据库
     *
     * @return
     */
    @ApiOperation(value = "列出hive上全部的数据库")
    @PostMapping("/allDatabases")
    @CrossOrigin
    public Result getAllDatabases() {
        List<String> strings = hiveMetaService.queryAllDataBases();
        return Result.ok(strings);
    }

    /**
     * 列出某个数据库中的全部表
     *
     * @param database
     * @return
     */
    @ApiOperation(value = "列出某个数据库中的全部表")
    @GetMapping("/{database}/allTables")
    @CrossOrigin
    public Result getAllTables(
            @ApiParam(name = "database", value = "数据库", required = true)
            @PathVariable String database) {
        List<String> strings = hiveMetaService.queryAllTables(database);
        return Result.ok(strings);
    }

    /**
     * 查询某张表的元数据
     */
    @ApiOperation(value = "查询某张表的元数据")
    @GetMapping("/{database}/{tableName}/detail")
    @CrossOrigin
    public Result getOneTableDetail(@ApiParam(name = "database", value = "数据库", required = true) @PathVariable String database,

                                    @ApiParam(name = "tableName", value = "表名", required = true) @PathVariable String tableName) {
        Table table = hiveMetaService.queryDetailOfOneTable(database, tableName);
        return Result.ok(table);
    }

}
