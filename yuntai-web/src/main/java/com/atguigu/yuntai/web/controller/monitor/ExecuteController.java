package com.atguigu.yuntai.web.controller.monitor;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.monitor.bean.PartitionDef;
import com.atguigu.yuntai.monitor.bean.PartitionResult;
import com.atguigu.yuntai.monitor.service.ExecuteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 执行与触发 前端控制器
 */
@RestController
@RequestMapping("/monitor/execute")
@Api(tags = "执行与触发")
//@CrossOrigin //跨域
@Slf4j
public class ExecuteController {

    @Autowired
    ExecuteService executeService;

    /**
     * 调试DSL
     * @param partitionDef
     * @return
     */
    @ApiOperation(value = "调试DSL")
    @PostMapping("/debugDsl")
    @CrossOrigin
    public Result debugDs(
            @ApiParam(name = "partitionDef", value = "分区表达式定", required = true)
                    PartitionDef partitionDef) {
        PartitionResult partitionResult = executeService.partitionDef2Result(partitionDef);
        return Result.ok(partitionResult.toString());
    }
}
