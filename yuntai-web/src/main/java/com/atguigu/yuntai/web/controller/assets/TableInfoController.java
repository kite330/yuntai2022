package com.atguigu.yuntai.web.controller.assets;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.govern.bean.QTable;
import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.service.TableInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 数据监控表信息 前端控制器
 */
@Api(tags = "数据监控表信息")
@RestController
@RequestMapping("/assets/tableInfo")
public class TableInfoController {
    @Autowired
    private TableInfoService tableInfoService;

    /**
     * 查询表信息集合
     * @return
     */
    @ApiOperation(value = "查询表信息集合")
    @GetMapping("/listTableInfo/{page}/{limit}")
    @CrossOrigin
    public Result listTableInfo(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,
            @ApiParam(name = "qTable", value = "查询条件", required = true)
                    QTable qTable) {
        Page<TableInfo> pageParam = new Page<>(page, limit);
        IPage<TableInfo> tableInfoListPage = null;
        //分页查询
        if(StringUtils.isNotEmpty(qTable.getSchemaName())&&StringUtils.isNotEmpty(qTable.getTableName())){
            tableInfoListPage = tableInfoService.page(pageParam,new QueryWrapper<TableInfo>().eq("schema_name",qTable.getSchemaName()).eq("table_name",qTable.getTableName()));
        }else if(StringUtils.isNotEmpty(qTable.getSchemaName())&&StringUtils.isEmpty(qTable.getTableName())){
            tableInfoListPage = tableInfoService.page(pageParam,new QueryWrapper<TableInfo>().eq("schema_name",qTable.getSchemaName()));
        }else if(StringUtils.isEmpty(qTable.getSchemaName())&&StringUtils.isNotEmpty(qTable.getTableName())){
            tableInfoListPage = tableInfoService.page(pageParam,new QueryWrapper<TableInfo>().eq("table_name",qTable.getTableName()));
        }else{
        tableInfoListPage = tableInfoService.page(pageParam);
        }
        return Result.ok(tableInfoListPage);
    }

    /**
     * 根据tableName查询表信息详情
     * @param tableName
     * @return
     */
    @ApiOperation(value = "根据tableName查询表信息详情")
    @GetMapping("/query/{tableName}")
    @CrossOrigin
    public Result tableInfoByName(
            @ApiParam(name = "tableName", value = "表名", required = true)
            @PathVariable String tableName){
        return Result.ok(tableInfoService.getOne(new QueryWrapper<TableInfo>().eq("table_name",tableName)));
    }

    /**
     * 查询条件集合
     * @return
     */
    @ApiOperation(value = "查询条件集合")
    @GetMapping("/queryList")
    @CrossOrigin
    public Result queryList(){
        List<TableInfo> tableInfoList = tableInfoService.list();
        //获取表集合
        List<String> tableNameList=tableInfoList.stream().map(TableInfo::getTableName).distinct().collect(Collectors.toList());
        //获取数据库集合
        List<String> schemaNameList=tableInfoList.stream().map(TableInfo::getSchemaName).distinct().collect(Collectors.toList());
        Map<String, List> stringListHashMap = new HashMap<>();
        stringListHashMap.put("tableNameList",tableNameList);
        stringListHashMap.put("schemaNameList",schemaNameList);
        return Result.ok(stringListHashMap);
    }

}
