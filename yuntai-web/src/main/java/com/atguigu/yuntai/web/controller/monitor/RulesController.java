package com.atguigu.yuntai.web.controller.monitor;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.monitor.entity.RuleCollection;
import com.atguigu.yuntai.monitor.entity.RuleInfo;
import com.atguigu.yuntai.monitor.service.RuleCollectionService;
import com.atguigu.yuntai.monitor.service.RuleInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 规则相关 前端控制器
 **/
@RestController
@RequestMapping("/monitor/rules")
@Api(tags = "规则相关")
//@CrossOrigin //跨域
@Slf4j
public class RulesController {

    @Autowired
    RuleCollectionService ruleCollectionService;

    @Autowired
    RuleInfoService ruleInfoService;

    /**
     * 创建一个规则集合
     * @param ruleCollection
     * @return
     */
    @ApiOperation(value = "创建一个规则集合")
    @PostMapping("/createCollection")
    @CrossOrigin
    public Result createOneCollection(@RequestBody RuleCollection ruleCollection) {
        ruleCollectionService.save(ruleCollection);
        return Result.ok();
    }

    /**
     * 查询这张表的全部规则集合
     * @param database
     * @param tableName
     * @return
     */
    @ApiOperation(value = "查询这张表的全部规则集合")
    @GetMapping("/queryAllRulesCollection/{database}/{tableName}")
    @CrossOrigin
    public Result getAllRulesCollection(
            @ApiParam(name = "database", value = "数据库名", required = true)
            @PathVariable String database,

            @ApiParam(name = "tableName", value = "表名", required = true)
            @PathVariable String tableName) {
        return Result.ok(ruleCollectionService.listCollectionsByHiveInfo(database,tableName));
    }

    /**
     * 删除一个规则集合
     * @param collectionId
     * @return
     */
    @ApiOperation(value = "删除一个规则集合")
    @DeleteMapping("/deleteOneCollection/{collectionId}")
    @CrossOrigin
    public Result deleteOneCollection(
            @ApiParam(name = "collectionId", value = "规则集合id", required = true)
            @PathVariable Long collectionId) {
        ruleCollectionService.removeOneCollectionById(collectionId);
        return Result.ok();
    }

    /**
     * 根据集合的id查询全部规则
     * @param collectionId
     * @return
     */
    @ApiOperation("根据集合的id查询全部规则")
    @GetMapping("/queryAllRules/{collectionId}")
    @CrossOrigin
    public Result getAllRules(
            @ApiParam(name = "collectionId", value = "规则集合id", required = true)
            @PathVariable Long collectionId) {
        return Result.ok(ruleInfoService.listAllRulesByCollectionId(collectionId));
    }

    /**
     * 添加或者更新一条规则
     * @param ruleInfo
     * @return
     */
    @ApiOperation( "添加或者更新一条规则")
    @PostMapping( "/saveOrUpdateOneRule")
    public Result saveOrUpdateOneRule(@RequestBody RuleInfo ruleInfo) {
        ruleInfoService.saveOrUpdateOneRuleInfo(ruleInfo);
        return Result.ok();
    }

    /**
     * 根据id删除一条规则
     * @param ruleId
     * @return
     */
    @ApiOperation( "根据id删除一条规则")
    @DeleteMapping( "/deleteOneRule/{ruleId}")
    public Result deleteOneRule(
            @ApiParam(name = "ruleId", value = "规则id", required = true)
            @PathVariable Long ruleId) {
        ruleInfoService.deleteOneRuleInfoById(ruleId);
        return Result.ok();
    }

    /**
     * 查询一条规则
     * @param ruleId
     * @return
     */
    @ApiOperation( "查询一条规则")
    @GetMapping( "/getOneRule/{ruleId}")
    public Result getOneRule(
            @ApiParam(name = "ruleId", value = "规则id", required = true)
            @PathVariable Long ruleId) {
        return Result.ok(ruleInfoService.getOneRuleInfoById(ruleId));
    }



}
