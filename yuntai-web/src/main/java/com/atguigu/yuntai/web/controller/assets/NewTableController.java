package com.atguigu.yuntai.web.controller.assets;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.service.TableInfoService;
import com.atguigu.yuntai.report.service.CustomQueryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @program: gmall
 * @description:
 */
@RestController
@RequestMapping("/assets/newTable")
public class NewTableController {
    @Autowired
    private CustomQueryService customQueryService;

    @Autowired
    private TableInfoService tableInfoService;


    /**
     * 创建Hive表
     * @return
     */
    @ApiOperation(value = "创建Hive表")
    @PostMapping("/createTable")
    @CrossOrigin
    public Result createTable(
            @ApiParam(name = "createTableBean", value = "建表", required = true)
            @RequestBody     TableInfo createTableBean) {
        //在hive中创建表
        customQueryService.createTable(createTableBean.getHiveSql());
        //将表信息记录到tableinfo中
//        tableInfoService.saveOrUpdateTableInfo(createTableBean);
        createTableBean.setCreateTime(new Date());
        if(createTableBean.getCreateTime()!=null){
            createTableBean.setUpdateTime(new Date());
        }
        tableInfoService.save(createTableBean);
        return Result.ok();
    }

}
