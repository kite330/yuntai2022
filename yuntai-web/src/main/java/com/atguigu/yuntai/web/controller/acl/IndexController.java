package com.atguigu.yuntai.web.controller.acl;

import com.atguigu.yuntai.acl.entity.Admin;
import com.atguigu.yuntai.acl.service.AdminService;
import com.atguigu.yuntai.acl.service.IndexService;
import com.atguigu.yuntai.acl.util.JwtHelper;
import com.atguigu.yuntai.common.util.MD5;
import com.atguigu.yuntai.common.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 后台登录与权限管理 前端控制器
 * </p>
 */
@RestController
@RequestMapping("/admin/acl/index")
@Api(tags = "后台登录与权限管理")
//@CrossOrigin //跨域
@Slf4j
public class IndexController {

    @Autowired
    private IndexService indexService;

    @Autowired
    private AdminService adminService;


    /**
     * 请求登陆的login
     *
     * @param adminVo
     * @return
     */
    @ApiOperation(value = "请求登陆的login")
    @PostMapping("login")
    public Result login(@RequestBody Admin adminVo) {
        //根据用户名获取用户信息
        Admin admin = adminService.selectByUsername(adminVo.getUsername());
        //密码对比
        if (!MD5.encrypt(adminVo.getPassword()).equals(admin.getPassword())) {
            return Result.fail("密码错误");
        }
        String token = JwtHelper.createToken(admin.getId(), admin.getUsername(), admin.getWarehouseId());
        return Result.ok(token);
    }

    /**
     * 根据token获取用户信息
     *
     * @param request
     * @return
     */
    @ApiOperation(value = "根据token获取用户信息")
    @GetMapping("info")
    public Result info(HttpServletRequest request) {
        //获取当前登录用户用户名
        String token = request.getHeader("token");
        String username = JwtHelper.getUserName(token);
        //根据用户名获取用户登录信息
        Map<String, Object> userInfo = indexService.getUserInfo(username);
        return Result.ok(userInfo);
    }

    /**
     * 获取菜单
     *
     * @param request
     * @return
     */
    @ApiOperation(value = "获取菜单")
    @GetMapping("menu")
    public Result getMenu(HttpServletRequest request) {
        //获取当前登录用户用户名
        String token = request.getHeader("token");
        String username = JwtHelper.getUserName(token);
        //根据用户名获取动态菜单
        Map<String, Object> menuMap = indexService.getMenu(username);
        return Result.ok(menuMap);
    }

    /**
     * 退出登陆
     *
     * @return
     */
    @ApiOperation(value = "退出登陆")
    @PostMapping("logout")
    public Result logout() {
        return Result.ok();
    }

}

