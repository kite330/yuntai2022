package com.atguigu.yuntai.web.controller.govern.bean;


import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class GovernanceSumScore {

    String scoreLevel;

    BigDecimal sumScore;

    List<BigDecimal> scoreList;

}
