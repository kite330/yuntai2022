package com.atguigu.yuntai.web.controller.statistics;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.statistics.bean.QActivity;
import com.atguigu.yuntai.statistics.entity.AdsActivityStats;
import com.atguigu.yuntai.statistics.service.ActivityService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 活动主题
 */
@RestController
@RequestMapping("/statistics/activity")
@Api(tags = "活动主题指标")
//@CrossOrigin //跨域
@Slf4j
public class ActivityController {

    @Autowired
    private ActivityService activityService;

    /**
     * 根据开始时间查询活动名称
     *
     * @param qActivity
     * @return
     */
    @ApiOperation(value = "根据开始时间查询活动名称")
    @GetMapping("getActivityNameByStartDate")
    @CrossOrigin
    public Result getActivityNameByStartDate(
            @ApiParam(name = "qActivity", value = "查询条件", required = true)
                    QActivity qActivity) {
        return Result.ok(activityService.getActivityNameByStartDate(qActivity));
    }

    /**
     * 根据开始时间和活动名称查询所有活动统计
     *
     * @param qActivity
     * @return
     */
    @ApiOperation(value = "根据开始时间和活动名称查询所有活动统计")
    @GetMapping("getActivityList/{page}/{limit}")
    @CrossOrigin
    public Result getActivityList(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,
            @ApiParam(name = "qActivity", value = "查询条件", required = true)
                    QActivity qActivity) {
        Page<AdsActivityStats> pageParam = new Page<>(page, limit);
        IPage<AdsActivityStats> pageModel = activityService.selectPage(pageParam, qActivity);
        return Result.ok(pageModel);
    }

}
