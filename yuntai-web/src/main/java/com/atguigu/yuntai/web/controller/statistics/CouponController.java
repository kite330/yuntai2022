package com.atguigu.yuntai.web.controller.statistics;


import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.statistics.bean.QCoupon;
import com.atguigu.yuntai.statistics.entity.AdsCouponStats;
import com.atguigu.yuntai.statistics.service.CouponService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 优惠卷主题
 */
@RestController
@RequestMapping("/statistics/coupon")
@Api(tags = "优惠卷主题指标")
//@CrossOrigin //跨域
@Slf4j
public class CouponController {

    @Autowired
    private CouponService couponService;

    /**
     * 根据开始时间查询优惠券名称
     *
     * @param qCoupon
     * @return
     */

    @ApiOperation(value = "根据开始时间查询优惠券名称")
    @GetMapping("getCouponNameByStartDate")
    @CrossOrigin
    public Result getCouponNameByStartDate(
            @ApiParam(name = "qCoupon", value = "查询条件", required = true)
                    QCoupon qCoupon) {
        return Result.ok(couponService.getCouponNameByStartDate(qCoupon));
    }

    /**
     * 根据开始时间和优惠券名称查询优惠券统计
     *
     * @param qCoupon
     * @return
     */
    @ApiOperation(value = "根据开始时间和优惠券名称查询优惠券统计")
    @GetMapping("getCouponList/{page}/{limit}")
    @CrossOrigin
    public Result getCouponList(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,
            @ApiParam(name = "qCoupon", value = "查询条件", required = true)
                    QCoupon qCoupon) {
        Page<AdsCouponStats> pageParam = new Page<>(page, limit);
        IPage<AdsCouponStats> pageModel = couponService.selectPage(pageParam, qCoupon);
        return Result.ok(pageModel);
    }

}
