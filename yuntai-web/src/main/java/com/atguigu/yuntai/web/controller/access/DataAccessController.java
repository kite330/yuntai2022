package com.atguigu.yuntai.web.controller.access;

import com.atguigu.yuntai.access.bean.ConnectorBean;
import com.atguigu.yuntai.access.service.DataAccessService;
import com.atguigu.yuntai.common.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("access")
@Api(tags = "用户管理")
//@CrossOrigin //跨域
@Slf4j
public class DataAccessController {

    @Autowired
    DataAccessService dataAccessService;

    /**
     * 新增连接
     * @param connector
     * @return
     */
    @ApiOperation(value = "新增Connector")
    @PostMapping( "registerConnector")
    @CrossOrigin
    public Result registerConnector(@RequestBody ConnectorBean connector) {
        return dataAccessService.registerConnector(connector);
    }

    /**
     * 删除Connector
     * @param name
     * @return
     */
    @ApiOperation(value = "删除Connector")
    @DeleteMapping( "deleteConnector/{name}")
    @CrossOrigin
    public Result deleteConnector(
            @ApiParam(name = "optionType", value = "治理新类型", required = true)
                                      @PathVariable  String name) {
        return dataAccessService.deleteConnector(name);
    }

    //暂停Connector
    @GetMapping( "pauseConnector/{name}")
    @CrossOrigin
    public Result pauseConnector(
            @ApiParam(name = "optionType", value = "治理新类型", required = true)
                                     @PathVariable  String name) {
        return dataAccessService.pauseConnector(name);
    }

    //恢复Connector
    @GetMapping( "resumeConnector/{name}")
    @CrossOrigin
    public Result resumeConnector(
            @ApiParam(name = "optionType", value = "治理新类型", required = true)
                                      @PathVariable String name) {
        return dataAccessService.resumeConnector(name);
    }

    //查询所有Connector
    @GetMapping("getConnectorList/{category}")
    @CrossOrigin
    public List<ConnectorBean> getConnectorList(
            @ApiParam(name = "optionType", value = "治理新类型", required = true)
             @PathVariable  String category) {
        return dataAccessService.getConnectorList(category);
    }

    //查询Connector详情
    @GetMapping( "getStatus/{name}")
    @CrossOrigin
    public ConnectorBean getStatus(
            @ApiParam(name = "optionType", value = "治理新类型", required = true)
                                       @PathVariable String name) {
        return dataAccessService.getStatus(name);
    }

    //更新Connector配置 Post
    @PutMapping( "updateConnector")
    @CrossOrigin
    public Result updateConnector(@RequestBody ConnectorBean connector) {
        return dataAccessService.updateConnector(connector);
    }
}
