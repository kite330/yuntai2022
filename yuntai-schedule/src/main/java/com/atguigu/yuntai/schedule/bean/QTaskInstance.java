package com.atguigu.yuntai.schedule.bean;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "任务查询条件")
public class QTaskInstance {


    private String name;

    /**
     * 状态
     */
    private int state;

    /**
     *工作流实例Id
     */
    private int processInstanceId;

    /**
     *
     */
    private String startTime;

    /**
     * 执行用户
     */
    private Long userId;

    private String sql;

    private List<Long> taskCodes;

    private String userName;


}
