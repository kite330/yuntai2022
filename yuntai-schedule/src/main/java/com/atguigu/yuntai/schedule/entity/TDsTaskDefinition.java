package com.atguigu.yuntai.schedule.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 任务定义实体类
 */
@Data
@ApiModel(description = "任务定义实体类")
@TableName("t_ds_task_definition")
@AllArgsConstructor
@NoArgsConstructor
public class TDsTaskDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * self-increasing id
     */
    @ApiModelProperty(value = "Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * encoding
     */
    @ApiModelProperty(value = "encoding")
    @TableField("code")
    private Long code;

    /**
     * task definition name
     */
     @ApiModelProperty(value = "任务定义名称")
    @TableField("name")
    private String name;

    /**
     * task definition version
     */
    @ApiModelProperty(value = "任务定义版本")
    @TableField("version")
    private Integer version;

    /**
     * description
     */
    @ApiModelProperty(value = "描述")
    @TableField("description")
    private String description;

    /**
     * project code
     */
    @ApiModelProperty(value = "项目code")
    @TableField("project_code")
    private Long projectCode;

    /**
     * task definition creator id
     */
    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private Integer userId;

    /**
     * task type
     */
    @ApiModelProperty(value = "任务类型")
    @TableField("task_type")
    private String taskType;

    /**
     * job custom parameters
     */
    @ApiModelProperty(value = "任务参数")
    @TableField("task_params")
    private String taskParams;

    /**
     * 0 not available, 1 available
     */
    @ApiModelProperty(value = "标志")
    @TableField("flag")
    private Integer flag;

    /**
     * job priority
     */
    @ApiModelProperty(value = "任务优先级")
    @TableField("task_priority")
    private Integer taskPriority;

    /**
     * worker grouping
     */
    @ApiModelProperty(value = "工作组")
    @TableField("worker_group")
    private String workerGroup;

    /**
     * environment code
     */
    @ApiModelProperty(value = "环境编码")
    @TableField("environment_code")
    private Long environmentCode;

    /**
     * number of failed retries
     */
    @ApiModelProperty(value = "故障恢复时间")
    @TableField("fail_retry_times")
    private Integer failRetryTimes;

    /**
     * failed retry interval
     */
    @ApiModelProperty(value = "故障恢复间隔")
    @TableField("fail_retry_interval")
    private Integer failRetryInterval;

    /**
     * timeout flag:0 close, 1 open
     */
    @ApiModelProperty(value = "超时标志")
    @TableField("timeout_flag")
    private Integer timeoutFlag;

    /**
     * timeout notification policy: 0 warning, 1 fail
     */
    @ApiModelProperty(value = "超时通知策略")
    @TableField("timeout_notify_strategy")
    private Integer timeoutNotifyStrategy;

    /**
     * timeout length,unit: minute
     */
    @ApiModelProperty(value = "超时时间")
    @TableField("timeout")
    private Integer timeout;

    /**
     * delay execution time,unit: minute
     */
    @ApiModelProperty(value = "延迟时间")
    @TableField("delay_time")
    private Integer delayTime;

    /**
     * resource id, separated by comma
     */
    @ApiModelProperty(value = "资源ids")
    @TableField("resource_ids")
    private String resourceIds;

    /**
     * create time
     */
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * update time
     */
    @ApiModelProperty(value = "修改时间")
    @TableField("update_time")
    private Date updateTime;


}
