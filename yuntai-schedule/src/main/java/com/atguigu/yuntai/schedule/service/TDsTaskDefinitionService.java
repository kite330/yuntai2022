package com.atguigu.yuntai.schedule.service;


import com.atguigu.yuntai.schedule.entity.TDsTaskDefinition;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 任务定义服务接口
 */

public interface TDsTaskDefinitionService extends IService<TDsTaskDefinition> {

    /**
     * 获取所有任务sql
     * @return
     */
    List<String> getAllTaskSql();

    /**
     * 根据表明获取任务sql
     * @param tableName
     * @return
     */
    String  getTaskSqlByName(String tableName);

    /**
     * 根据表明获取任务定义
     * @param tableName
     * @return
     */
    TDsTaskDefinition getReleastTaskDefinitionByName(String tableName);
}
