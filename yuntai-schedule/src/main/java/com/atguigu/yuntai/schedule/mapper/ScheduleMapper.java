package com.atguigu.yuntai.schedule.mapper;

import com.atguigu.yuntai.schedule.bean.QTaskInstance;
import com.atguigu.yuntai.schedule.bean.TaskInstance;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigInteger;
import java.util.List;

@DS("mysql-ds")
public interface ScheduleMapper extends BaseMapper<TaskInstance> {

    @Select("<script> " +
            "select * from " +
            "(" +
            "select vtd.code,vtd.name,vtp.name projectName,vtd.task_type taskType,vtu.user_name userName,vti.end_time endTime,IFNULL(vti.state,12) as state " +
            "from t_ds_task_definition vtd " +
            "left join t_ds_project vtp on vtd.project_code = vtp.code " +
            "left join t_ds_user vtu on vtd.user_id = vtu.id " +
            "left join t_ds_task_instance vti on vtd.code = vti.task_code  " +
            "<where> 1=1 and vtd.code in " +
            "<foreach collection='vo.taskCodes' item = 'taskCode' index='index' open='(' close=')' separator=','>" +
            "#{taskCode}</foreach>" +
            "<if test='vo.userId != 0'> " +
            "and vtu.id = #{vo.userId} " +
            "</if> " +
            "<if test='vo.state >-1'> " +
            "and vti.state = #{vo.state} " +
            "</if> " +
            "<if test='vo.name != null'> " +
            "and vtd.name like  CONCAT('%',#{vo.name},'%') " +
            "</if> " +
            "</where>" +
            "HAVING 1 ORDER BY vti.end_time desc ) t  group by code order by endTime desc" +
            "</script> ")
    IPage<TaskInstance> getScheduleList(Page<TaskInstance> page, @Param("vo") QTaskInstance qTaskInstance);

//    @Select("select count(*) from task_instance where task_instance_id = #{taskInstanceId}")
//    int getTaskInstanceCount(int taskInstanceId);
//
//    @Insert("insert into task_instance(task_instance_id,expect_time) values (#{taskInstanceId},#{expectTime})")
//    void insertJobSchedule(TaskInstance vTaskInstance);
//
//    @Update("update task_instance set expect_time = #{expectTime} where task_instance_id = #{taskInstanceId}")
//    void updateJobSchedule(TaskInstance vTaskInstance);

    @Select("select vtd.code,vtd.name,vtd.task_params taskParams,vtd.fail_retry_times failRetryTimes,vtd.fail_retry_interval failRetryInterval,vtd.delay_time delayTime, " +
            "vtd.timeout_flag timeoutFlag,vtd.timeout_notify_strategy timeoutNotifyStrategy,vtd.timeout timeout, vti.submit_time submitTime,vti.start_time startTime,vti.end_time endTime,vti.state state \n" +
            "from t_ds_task_definition vtd \n" +
            "left join t_ds_project vtp on vtd.project_code = vtp.code\n" +
            "left join t_ds_user vtu on vtd.user_id = vtu.id\n" +
            "left join t_ds_task_instance vti on vtd.code = vti.task_code where vtd.code = #{code} order by vti.end_time desc limit 1;")
    TaskInstance getSchedelDetailById(BigInteger code);

    @Select("select vtd.code,vtd.name,vtd.task_params taskParams,vtd.fail_retry_times failRetryTimes,vtd.fail_retry_interval failRetryInterval,vtd.delay_time delayTime, " +
            "vtd.timeout_flag timeoutFlag,vtd.timeout_notify_strategy timeoutNotifyStrategy,vtd.timeout timeout, vti.submit_time submitTime,vti.start_time startTime,vti.end_time endTime,vti.state state,vtu.user_name userName \n" +
            "from t_ds_task_definition vtd \n" +
            "left join t_ds_project vtp on vtd.project_code = vtp.code\n" +
            "left join t_ds_user vtu on vtd.user_id = vtu.id\n" +
            "left join t_ds_task_instance vti on vtd.code = vti.task_code where vtd.name = #{name} order by vti.end_time desc limit 1;")
    TaskInstance getSchedelDetailByName(String name);
}
