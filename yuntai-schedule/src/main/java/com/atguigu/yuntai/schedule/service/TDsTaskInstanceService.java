package com.atguigu.yuntai.schedule.service;


import com.atguigu.yuntai.schedule.entity.TDsTaskInstance;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 任务实例服务接口
 */
public interface TDsTaskInstanceService extends IService<TDsTaskInstance> {

    /**
     *根据表名获取错误任务数
     * @param busiDate
     * @param tableName
     * @return
     */
     Long countErrorTaskByTableName( String busiDate,  String tableName);

    /**
     *根据表名和开始时间获取成功任务
     * @param busiDate
     * @param tableName
     * @return
     */
     TDsTaskInstance getSuccessTaskByTableName(String busiDate, String tableName);

    /**
     *根据表名获取一段时间成功任务
     * @param busiDate
     * @param tableName
     * @param withRecentDays
     * @return
     */
     List<TDsTaskInstance> getSuccessTaskListByTableName(String busiDate, String tableName, Integer withRecentDays);
}
