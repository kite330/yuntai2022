package com.atguigu.yuntai.schedule.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *ds用户
 * </p>
 */
/**
 * 任务实例
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("ds 用户实例")
@TableName("t_ds_user")
public class TDsUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * user id
     */
    @ApiModelProperty(value = "ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * user name
     */
    @ApiModelProperty(value = "用户名称")
    @TableField("user_name")
    private String userName;

    /**
     * user password
     */
    @ApiModelProperty(value = "用户密码")
    @TableField("user_password")
    private String userPassword;

    /**
     * user type, 0:administrator，1:ordinary user
     */
    @ApiModelProperty(value = "用户类型")
    @TableField("user_type")
    private Integer userType;

    /**
     * email
     */
    @ApiModelProperty(value = "邮箱")
    @TableField("email")
    private String email;

    /**
     * phone
     */
    @ApiModelProperty(value = "电话")
    @TableField("phone")
    private String phone;

    /**
     * tenant id
     */
    @ApiModelProperty(value = "租户id")
    @TableField("tenant_id")
    private Integer tenantId;

    /**
     * create time
     */
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * update time
     */
    @ApiModelProperty(value = "修改时间")
    @TableField("update_time")
    private Date updateTime;

    /**
     * queue
     */
    @ApiModelProperty(value = "队列")
    @TableField("queue")
    private String queue;

    /**
     * state 0:disable 1:enable
     */
    @ApiModelProperty(value = "状态")
    @TableField("state")
    private Integer state;




    @Override
    public String toString() {
        return "TDsUser{" +
        "id=" + id +
        ", userName=" + userName +
        ", userPassword=" + userPassword +
        ", userType=" + userType +
        ", email=" + email +
        ", phone=" + phone +
        ", tenantId=" + tenantId +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", queue=" + queue +
        ", state=" + state +
        "}";
    }
}
