package com.atguigu.yuntai.schedule.service;

import java.util.List;

public interface TDsProcessDefinitionService {

    /**
     * 获取taskcode
     * @return
     */
    List<Long> getReleastCodeList();
}
