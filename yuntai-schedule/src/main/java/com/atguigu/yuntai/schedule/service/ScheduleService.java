package com.atguigu.yuntai.schedule.service;

import com.atguigu.yuntai.schedule.bean.QTaskInstance;
import com.atguigu.yuntai.schedule.bean.TaskInstance;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.math.BigInteger;
import java.util.List;

public interface ScheduleService {

    IPage<TaskInstance> getScheduleList(Page<TaskInstance> pageParam, QTaskInstance qschedule);

//    void updateSchedule(TaskInstance vTaskInstance);

    TaskInstance getSchedelDetailById(BigInteger code);

    TaskInstance getSchedelDetailByName(String name);
}
