package com.atguigu.yuntai.schedule.mapper;

import com.atguigu.yuntai.schedule.entity.TDsProcessDefinition;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@DS("mysql-ds")
public interface TDsProcessDefinitionMapper extends BaseMapper<TDsProcessDefinition> {

    @Select("select JSON_EXTRACT( locations,'$[*].taskCode') from t_ds_process_definition where release_state=1")
    public List<String> getTaskCodes();
}
