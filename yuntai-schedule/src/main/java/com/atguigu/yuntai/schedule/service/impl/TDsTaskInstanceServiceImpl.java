package com.atguigu.yuntai.schedule.service.impl;


import com.atguigu.yuntai.schedule.entity.TDsTaskInstance;
import com.atguigu.yuntai.schedule.mapper.TDsTaskInstanceMapper;
import com.atguigu.yuntai.schedule.service.TDsTaskInstanceService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 任务实例实现类
 */
@Service
public class TDsTaskInstanceServiceImpl extends ServiceImpl<TDsTaskInstanceMapper, TDsTaskInstance> implements TDsTaskInstanceService {


    @Autowired
    private TDsTaskInstanceMapper taskInstanceMapper;

    public Long countErrorTaskByTableName(String busiDate, String tableName) {
        return taskInstanceMapper.countErrorTaskByTableName(busiDate, tableName);
    }

    public TDsTaskInstance getSuccessTaskByTableName(String busiDate, String tableName) {
        TDsTaskInstance taskInstance = getOne(new QueryWrapper<TDsTaskInstance>()
                .eq("DATE_FORMAT(start_time,'%Y-%m-%d')", busiDate)
                .eq("state", 7).eq("name", tableName));
        return taskInstance;
    }

    public List<TDsTaskInstance> getSuccessTaskListByTableName(String busiDate, String tableName, Integer withRecentDays) {
        List<TDsTaskInstance> taskInstanceList = list(new QueryWrapper<TDsTaskInstance>()
                .le("datediff(start_time,'" + busiDate + "')", withRecentDays)
                .eq("state", 7).eq("name", tableName));
        return taskInstanceList;
    }
}
