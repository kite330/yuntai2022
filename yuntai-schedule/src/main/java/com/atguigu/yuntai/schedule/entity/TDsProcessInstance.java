package com.atguigu.yuntai.schedule.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Objects;

/**
 * ds进程实例
 */
@Data
@ApiModel(description = "ds进程实例")
@TableName("t_ds_process_instance")
@AllArgsConstructor
@NoArgsConstructor
public class TDsProcessInstance {
    /**
     * id
     */
    @ApiModelProperty(value = "Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 进程名称
     */
    @ApiModelProperty(value = "进程名称")
    @TableField("name")
    private String name;
    /**
     * 进程定义编码
     */
    @ApiModelProperty(value = "进程定义编码")
    @TableField("process_definition_code")
    private Long processDefinitionCode;
    /**
     * 进程定义版本
     */
    @ApiModelProperty(value = "进程定义版本")
    @TableField("process_definition_version")
    private Integer processDefinitionVersion;
    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    @TableField("state")
    private Byte state;
    /**
     * 恢复
     */
    @ApiModelProperty(value = "恢复")
    @TableField("recovery")
    private Byte recovery;
    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    @TableField("start_time")
    private Timestamp startTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "任务定义名称")
    @TableField("end_time")
    private Timestamp endTime;
    /**
     * 运行时间
     */
    @ApiModelProperty(value = "运行时间")
    @TableField("run_times")
    private Integer runTimes;
    /**
     * 服务器
     */
    @ApiModelProperty(value = "服务器")
    @TableField("host")
    private String host;
    /**
     * 命令类型
     */
    @ApiModelProperty(value = "命令类型")
    @TableField("command_type")
    private Byte commandType;
    /**
     * 命令参数
     */
    @ApiModelProperty(value = "命令参数")
    @TableField("command_param")
    private String commandParam;
    /**
     * 任务依赖参数
     */
    @ApiModelProperty(value = "任务定义名称")
    @TableField("task_depend_type")
    private Byte taskDependType;
    /**
     * 最大尝试次数
     */
    @ApiModelProperty(value = "最大尝试次数")
    @TableField("max_try_times")
    private Byte maxTryTimes;
    /**
     * 失败策略
     */
    @ApiModelProperty(value = "失败策略")
    @TableField("failure_strategy")
    private Byte failureStrategy;
    /**
     * 警告类型
     */
    @ApiModelProperty(value = "警告类型")
    @TableField("warning_type")
    private Byte warningType;
    /**
     * 告警组
     */
    @ApiModelProperty(value = "告警组")
    @TableField("warning_group_id")
    private Integer warningGroupId;
    /**
     * 计划时间
     */
    @ApiModelProperty(value = "计划时间")
    @TableField("schedule_time")
    private Timestamp scheduleTime;
    /**
     * 命令开始时间
     */
    @ApiModelProperty(value = "命令开始时间")
    @TableField("command_start_time")
    private Timestamp commandStartTime;
    /**
     *全局参数
     */
    @ApiModelProperty(value = "全局参数")
    @TableField("global_params")
    private String globalParams;
    /**
     * 标志
     */
    @ApiModelProperty(value = "标志")
    @TableField("flag")
    private Byte flag;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    @TableField("update_time")
    private Timestamp updateTime;
    /**
     * 子流程
     */
    @ApiModelProperty(value = "子流程")
    @TableField("is_sub_process")
    private Integer isSubProcess;
    /**
     * 执行人Id
     */
    @ApiModelProperty(value = "执行人Id")
    @TableField("executor_id")
    private Integer executorId;
    /**
     * 历史命令
     */
    @ApiModelProperty(value = "历史命令")
    @TableField("history_cmd")
    private String historyCmd;
    /**
     * 流程实例优先级
     */
    @ApiModelProperty(value = "流程实例优先级")
    @TableField("process_instance_priority")
    private Integer processInstancePriority;
    /**
     * 工作组
     */
    @ApiModelProperty(value = "工作组")
    @TableField("worker_group")
    private String workerGroup;
    /**
     * 环境编码
     */
    @ApiModelProperty(value = "环境编码")
    @TableField("environment_code")
    private Long environmentCode;
    /**
     * 超时时长
     */
    @ApiModelProperty(value = "超时时长")
    @TableField("timeout")
    private Integer timeout;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    @TableField("tenant_id")
    private Integer tenantId;
    /**
     * var_pool
     */
    @ApiModelProperty(value = "var_pool")
    @TableField("var_pool")
    private String varPool;
    @ApiModelProperty(value = "任务定义名称")
    /**
     * dry run flag：0 normal, 1 dry run
     */
    @TableField("dry_run")
    private Byte dryRun;
    /**
     * 串行队列下一个进程实例ID
     */
    @ApiModelProperty(value = "串行队列下一个进程实例ID")
    @TableField("next_process_instance_id")
    private Integer nextProcessInstanceId;
    /**
     * 流程实例重启时间
     */
    @ApiModelProperty(value = "流程实例重启时间")
    @TableField("restart_time")
    private Timestamp restartTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TDsProcessInstance that = (TDsProcessInstance) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(processDefinitionCode, that.processDefinitionCode) && Objects.equals(processDefinitionVersion, that.processDefinitionVersion) && Objects.equals(state, that.state) && Objects.equals(recovery, that.recovery) && Objects.equals(startTime, that.startTime) && Objects.equals(endTime, that.endTime) && Objects.equals(runTimes, that.runTimes) && Objects.equals(host, that.host) && Objects.equals(commandType, that.commandType) && Objects.equals(commandParam, that.commandParam) && Objects.equals(taskDependType, that.taskDependType) && Objects.equals(maxTryTimes, that.maxTryTimes) && Objects.equals(failureStrategy, that.failureStrategy) && Objects.equals(warningType, that.warningType) && Objects.equals(warningGroupId, that.warningGroupId) && Objects.equals(scheduleTime, that.scheduleTime) && Objects.equals(commandStartTime, that.commandStartTime) && Objects.equals(globalParams, that.globalParams) && Objects.equals(flag, that.flag) && Objects.equals(updateTime, that.updateTime) && Objects.equals(isSubProcess, that.isSubProcess) && Objects.equals(executorId, that.executorId) && Objects.equals(historyCmd, that.historyCmd) && Objects.equals(processInstancePriority, that.processInstancePriority) && Objects.equals(workerGroup, that.workerGroup) && Objects.equals(environmentCode, that.environmentCode) && Objects.equals(timeout, that.timeout) && Objects.equals(tenantId, that.tenantId) && Objects.equals(varPool, that.varPool) && Objects.equals(dryRun, that.dryRun) && Objects.equals(nextProcessInstanceId, that.nextProcessInstanceId) && Objects.equals(restartTime, that.restartTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, processDefinitionCode, processDefinitionVersion, state, recovery, startTime, endTime, runTimes, host, commandType, commandParam, taskDependType, maxTryTimes, failureStrategy, warningType, warningGroupId, scheduleTime, commandStartTime, globalParams, flag, updateTime, isSubProcess, executorId, historyCmd, processInstancePriority, workerGroup, environmentCode, timeout, tenantId, varPool, dryRun, nextProcessInstanceId, restartTime);
    }
}
