package com.atguigu.yuntai.schedule.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 任务实例
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("ds 任务实例")
@TableName("t_ds_task_instance")
public class TDsTaskInstance implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * key
     */
    @ApiModelProperty(value = "Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * task name
     */
    @ApiModelProperty(value = "任务名称")
    @TableField("name")
    private String name;

    /**
     * task type
     */
    @ApiModelProperty(value = "任务类型")
    @TableField("task_type")
    private String taskType;

    /**
     * task definition code
     */
    @ApiModelProperty(value = "任务编码")
    @TableField("task_code")
    private Long taskCode;

    /**
     * task definition version
     */
    @ApiModelProperty(value = "任务定义版本")
    @TableField("task_definition_version")
    private Integer taskDefinitionVersion;

    /**
     * process instance id
     */
    @ApiModelProperty(value = "进程起始id")
    @TableField("process_instance_id")
    private Integer processInstanceId;

    /**
     * Status: 0 commit succeeded, 1 running, 2 prepare to pause, 3 pause, 4 prepare to stop, 5 stop, 6 fail, 7 succeed, 8 need fault tolerance, 9 kill, 10 wait for thread, 11 wait for dependency to complete
     */
    @ApiModelProperty(value = "状态")
    @TableField("state")
    private Integer state;

    /**
     * task submit time
     */
    @ApiModelProperty(value = "提交时间")
    @TableField("submit_time")
    private Date submitTime;

    /**
     * task start time
     */
    @ApiModelProperty(value = "开始时间")
    @TableField("start_time")
    private Date startTime;

    /**
     * task end time
     */
    @ApiModelProperty(value = "结束时间")
    @TableField("end_time")
    private Date endTime;

    /**
     * host of task running on
     */
    @ApiModelProperty(value = "任务主机")
    @TableField("host")
    private String host;

    /**
     * task execute path in the host
     */
    @ApiModelProperty(value = "执行路径")
    @TableField("execute_path")
    private String executePath;

    /**
     * task log path
     */
    @ApiModelProperty(value = "日志路径")
    @TableField("log_path")
    private String logPath;

    /**
     * whether alert
     */
    @ApiModelProperty(value = "报警标志")
    @TableField("alert_flag")
    private Integer alertFlag;

    /**
     * task retry times
     */
    @ApiModelProperty(value = "重试次数")
    @TableField("retry_times")
    private Integer retryTimes;

    /**
     * pid of task
     */
    @ApiModelProperty(value = "pid")
    @TableField("pid")
    private Integer pid;

    /**
     * yarn app id
     */
    @ApiModelProperty(value = "应用链接")
    @TableField("app_link")
    private String appLink;

    /**
     * job custom parameters
     */
    @ApiModelProperty(value = "任务参数")
    @TableField("task_params")
    private String taskParams;

    /**
     * 0 not available, 1 available
     */
    @ApiModelProperty(value = "标志")
    @TableField("flag")
    private Integer flag;

    /**
     * retry interval when task failed
     */
    @ApiModelProperty(value = "重试间隔")
    @TableField("retry_interval")
    private Integer retryInterval;

    /**
     * max retry times
     */
    @ApiModelProperty(value = "最大重试时间")
    @TableField("max_retry_times")
    private Integer maxRetryTimes;

    /**
     * task instance priority:0 Highest,1 High,2 Medium,3 Low,4 Lowest
     */
    @ApiModelProperty(value = "任务瞬时优先级")
    @TableField("task_instance_priority")
    private Integer taskInstancePriority;

    /**
     * worker group id
     */
    @ApiModelProperty(value = "工作组")
    @TableField("worker_group")
    private String workerGroup;

    /**
     * environment code
     */
    @ApiModelProperty(value = "环境code")
    @TableField("environment_code")
    private Long environmentCode;

    /**
     * this config contains many environment variables config
     */
    @ApiModelProperty(value = "环境配置")
    @TableField("environment_config")
    private String environmentConfig;

    @ApiModelProperty(value = "执行者ID")
    @TableField("executor_id")
    private Integer executorId;

    /**
     * task first submit time
     */
    @ApiModelProperty(value = "第一次提交时间")
    @TableField("first_submit_time")
    private Date firstSubmitTime;

    /**
     * task delay execution time
     */
    @ApiModelProperty(value = "延迟时间")
    @TableField("delay_time")
    private Integer delayTime;

    /**
     * var_pool
     */
    @ApiModelProperty(value = "var_pool")
    @TableField("var_pool")
    private String varPool;

    /**
     * dry run flag: 0 normal, 1 dry run
     */
    @ApiModelProperty(value = "dry_run")
    @TableField("dry_run")
    private Integer dryRun;



}
