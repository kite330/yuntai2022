package com.atguigu.yuntai.monitor.service.impl;

import com.atguigu.yuntai.monitor.bean.TriggerLogRowVO;
import com.atguigu.yuntai.monitor.entity.TriggerLog;
import com.atguigu.yuntai.monitor.mapper.TriggerLogMapper;
import com.atguigu.yuntai.monitor.service.TriggerLogService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>TriggerLogService的实现类</p>
 **/
@Service
public class TriggerLogServiceImpl extends ServiceImpl<TriggerLogMapper, TriggerLog> implements TriggerLogService {

    @Override
    public IPage<TriggerLogRowVO> listTriggerLogRowVO(Page<TriggerLogRowVO> pageParam) {
        return getBaseMapper().listTriggerLogRowVO(pageParam);
    }

    @Override
    public void setIfAlertTrueById(Long id) {
        this.update()
                .eq("id",id)
                .set("if_alert",true)
                .update();
    }

//    @Override
//    public TriggerLogVO getTriggerLogVO() {
//        return new TriggerLogVO((long) count(),listTriggerLogRowVO(pageStart,pageEnd));
//    }

    @Override
    public void addOneTriggerLog(TriggerLog triggerLog) {
        save(triggerLog);
    }

    @Override
    public void updateStatueById(Long triggerLogId,String statue) {
        update()
                .eq("id",triggerLogId)
                .set("statue",statue)
                .update()
                ;
    }
}
