package com.atguigu.yuntai.monitor.service.impl;


import com.atguigu.yuntai.monitor.bean.AlertMsgRowVO;
import com.atguigu.yuntai.monitor.entity.AlertMsg;
import com.atguigu.yuntai.monitor.mapper.AlertMsgMapper;
import com.atguigu.yuntai.monitor.service.AlertMsgService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>AlertMsgService实现类</p>
 **/
@Service
public class AlertMsgServiceImpl extends ServiceImpl<AlertMsgMapper, AlertMsg> implements AlertMsgService {

    /**
     * 获取未读报警信息的条数
     * @return
     */
    @Override
    public Integer countUnread(){
        return query()
                .eq("if_read", false)
                .count()
                ;
    }


    @Override
    public List<AlertMsgRowVO> listAlertMsgRowVoByTriggerId(Long triggerId) {
        return getBaseMapper().listAlertMsgRowVo(triggerId);
    }

    /**
     * 标记这些已经读过了
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void read(List<Long> ids) {
        for (Long id : ids) {
            boolean b = update()
                        .eq("id",id)
                        .set("if_read",true)
                        .update()
                        ;
            if(!b){
                throw new RuntimeException("标记已读有失败");
            }
        }
    }

    @Override
    public List<AlertMsg> listAlertMsgByTriggerId(Long triggerId) {
        return query()
                .eq("trigger_log_id",triggerId)
                .orderByDesc("id")
                .list();
    }

    @Override
    public IPage<AlertMsgRowVO> getAlertMsgVO(Page<AlertMsgRowVO> pageParam) {
        return baseMapper.listAlertMsgRowVO(pageParam);
    }

    @Override
    public void updateIfSendByTriggerId(Long triggerLogId) {
        update()
                .eq("collection_id",triggerLogId)
                .set("if_send",true)
                .update();
    }

    @Override
    public void updateSendById(Long id) {
        boolean b = update()
                .eq("id",id)
                .set("if_read",true)
                .update()
                ;
        if(!b){
            throw new RuntimeException("标记已读有失败");
        }
    }
}
