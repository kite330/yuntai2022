package com.atguigu.yuntai.monitor.exception;

/**
 * <p>DSL的异常</p>
 **/
public class DslException extends Exception {
    public DslException(String msg,Throwable e) {
        super(msg,e);
    }
}
