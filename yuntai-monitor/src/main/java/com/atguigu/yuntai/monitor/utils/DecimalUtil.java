package com.atguigu.yuntai.monitor.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * <p></p>

 **/
public class DecimalUtil {

    public static BigDecimal upper19d6 = BigDecimal.valueOf(Double.MAX_VALUE).setScale(6,RoundingMode.HALF_UP);

    public static BigDecimal lower19d6 = BigDecimal.valueOf(Double.MIN_VALUE).setScale(6,RoundingMode.HALF_UP);

    public static BigDecimal toDecimal19d6(String value) {
        if(value==null){
            return null;
        }
        return new BigDecimal(value).setScale(6, RoundingMode.HALF_UP);
    }

    public static String getString(BigDecimal bigDecimal) {
        if(bigDecimal == null){
            return "null";
        }
        return bigDecimal.stripTrailingZeros().toString();
    }


}
