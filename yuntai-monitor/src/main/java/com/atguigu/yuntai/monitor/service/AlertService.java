package com.atguigu.yuntai.monitor.service;

/**
 * <p>报警服务</p>
 *
 **/
public interface AlertService {

    /**
     * 跟触发有关，但是和具体规则无关的报警
     * 比如执行中遇到程序异常等等
     * @param triggerLogId 触发日志的id
     * @param collectionId 规则组的id
     * @param msg 报警消息
     */
    void alert(Long triggerLogId, Long collectionId, String msg);

    /**
     * 和规则相关的报警,有具体的某个规则没有满足要求
     * @param triggerLogId 触发日志的id
     * @param collectionId 规则组的id
     * @param ruleInfoComputeId 规则计算结果的id
     * @param msg 报警信息
     */
    void alert(Long triggerLogId,Long collectionId, Long ruleInfoComputeId,  String msg);

    void triggerAlert(Long triggerLogId);
}
