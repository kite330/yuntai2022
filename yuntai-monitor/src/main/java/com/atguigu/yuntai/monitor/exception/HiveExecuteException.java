package com.atguigu.yuntai.monitor.exception;

/**
 * <p></p>
 **/
public class HiveExecuteException extends Exception {
    public HiveExecuteException(String msg,Throwable e){
        super(msg,e);
    }
}
