package com.atguigu.yuntai.monitor.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * <p>触发执行日志</p>
 *
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TriggerLog {

    public TriggerLog(String who,Long collectionId) {
        this.who = who;
        this.collectionId = collectionId;
    }

    /**
     * 自增id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 规则集合id
     */
    private Long collectionId;

    /**
     * 触发者
     */
    private String who;

    /**
     * 是否触发了报警
     */
    private Boolean ifAlert;

    /**
     * 触发时间
     */
    private Date triggerTime;

    /**
     * 任务状态, 创建,成功,失败
     */
    private String statue;

    /**
     * 完成时间
     */
    private Date computedTime;
}
