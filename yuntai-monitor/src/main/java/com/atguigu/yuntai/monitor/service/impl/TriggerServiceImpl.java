package com.atguigu.yuntai.monitor.service.impl;

import com.atguigu.yuntai.monitor.entity.TriggerLog;
import com.atguigu.yuntai.monitor.service.ExecuteService;
import com.atguigu.yuntai.monitor.service.TriggerLogService;
import com.atguigu.yuntai.monitor.service.TriggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>TriggerService的实现类</p>
 *
 **/
@Service
public class TriggerServiceImpl implements TriggerService {

    @Autowired
    TriggerLogService triggerLogService;

    @Autowired
    ExecuteService executeService;


    @Override
    public void triggerCollection(String who, Long collectionId) {
        // 0. 创建一个触发日志
        TriggerLog triggerLog = new TriggerLog(who, collectionId);
        // 1. 保存一条触发日志
        triggerLogService.addOneTriggerLog(triggerLog);
        // 2. 开始执行
        executeService.execute(triggerLog.getId(),collectionId);
    }
}






