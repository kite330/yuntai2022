package com.atguigu.yuntai.monitor.service;

import com.atguigu.yuntai.monitor.bean.AlertMsgRowVO;
import com.atguigu.yuntai.monitor.entity.AlertMsg;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 报警信息Service
 */
public interface AlertMsgService extends IService<AlertMsg> {
    Integer countUnread();

//    List<AlertMsgRowVO> listAlertMsgRowVO(Integer pageStart, Integer pageSize);

    List<AlertMsgRowVO> listAlertMsgRowVoByTriggerId(Long triggerId);

    IPage<AlertMsgRowVO> getAlertMsgVO(Page<AlertMsgRowVO> pageParam);

    @Transactional
    void read(List<Long> ids);

    List<AlertMsg> listAlertMsgByTriggerId(Long triggerId);

    void updateIfSendByTriggerId(Long triggerLogId);

    /**
     * 单个修改报警阅读状态
     * @param id
     */
    void updateSendById(Long id);
}
