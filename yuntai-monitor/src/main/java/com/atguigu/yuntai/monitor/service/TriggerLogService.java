package com.atguigu.yuntai.monitor.service;

import com.atguigu.yuntai.monitor.bean.TriggerLogRowVO;
import com.atguigu.yuntai.monitor.entity.TriggerLog;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 数据服务层，触发日志
 *
 */
public interface TriggerLogService extends IService<TriggerLog> {

    /**
     * 根据id将是否报警更改为true
     * @param id 触发记录的id
     */
    void setIfAlertTrueById(Long id);

    IPage<TriggerLogRowVO> listTriggerLogRowVO(Page<TriggerLogRowVO> pageParam);

    void addOneTriggerLog(TriggerLog triggerLog);

    void updateStatueById(Long triggerLogId,String statue);
}
