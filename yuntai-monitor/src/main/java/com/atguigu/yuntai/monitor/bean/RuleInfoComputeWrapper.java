package com.atguigu.yuntai.monitor.bean;

import com.atguigu.yuntai.monitor.entity.RuleInfoCompute;
import lombok.Data;

/**
 * <p>计算对象的包装</p>
 **/
@Data
public class RuleInfoComputeWrapper {

    /**
     * 对应的分区类型，基准分区还是受检分区
     */
    private String type;

    private RuleInfoCompute ruleInfoCompute;

    public RuleInfoComputeWrapper(String type,RuleInfoCompute ruleInfoCompute){
        this.type = type;
        this.ruleInfoCompute = ruleInfoCompute;
    }

}
