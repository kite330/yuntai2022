package com.atguigu.yuntai.monitor.service;

import com.atguigu.yuntai.monitor.bean.RuleInfoComputeVO;
import com.atguigu.yuntai.monitor.entity.RuleInfoCompute;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *
 */
public interface RuleInfoComputeService extends IService<RuleInfoCompute> {

    void saveRuleInfoComputeList(List<RuleInfoCompute> ruleInfoComputeList);

    RuleInfoComputeVO getRuleInfoComputeVOByTriggerId(Long triggerId);
}
