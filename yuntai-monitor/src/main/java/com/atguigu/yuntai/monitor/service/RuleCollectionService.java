package com.atguigu.yuntai.monitor.service;

import com.atguigu.yuntai.monitor.entity.RuleCollection;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 规则集合相关服务接口
 */
public interface RuleCollectionService extends IService<RuleCollection> {

    /**
     * 根据规则集合id删除集合
     * @param collectionId
     */
    void removeOneCollectionById(Long collectionId);

    /**
     * 创建一个规则集合
     * @param ruleCollection
     */
    void createOneCollection(RuleCollection ruleCollection);

    /**
     * 根据数据库和表查询所有规则集合
     * @param database
     * @param tableName
     * @return
     */
    List<RuleCollection> listCollectionsByHiveInfo(String database, String tableName);

}
