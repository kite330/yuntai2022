package com.atguigu.yuntai.monitor.bean;

import com.atguigu.yuntai.monitor.entity.AlertMsg;
import com.atguigu.yuntai.monitor.entity.RuleInfoCompute;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * <p>除了规则的执行情况，还要再带上报警信息</p>
 *
 **/
@Data
@AllArgsConstructor
public class RuleInfoComputeRowVO {

    private RuleInfoCompute ruleInfoCompute;

    private List<AlertMsg> alertMsgList;


}
