package com.atguigu.yuntai.monitor.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * <p>报警消息记录</p>
 *
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlertMsg {

    /**
     * 这个构造器用于那些和规则监控无关的报警，比如sql执行失败
     * @param triggerLogId
     * @param msg
     */
    public AlertMsg(Long triggerLogId, String msg) {
        this.triggerLogId = triggerLogId;
        this.msg = msg;
    }

    /**
     * 这个构造器用于和规则相关的报警，比如哪个不合规了
     * @param triggerLogId
     * @param ruleInfoComputeId
     * @param msg
     */
    public AlertMsg(Long triggerLogId, Long collectionId, Long ruleInfoComputeId, String msg) {
        this.triggerLogId = triggerLogId;
        this.collectionId = collectionId;
        this.ruleInfoComputeId = ruleInfoComputeId;
        this.msg = msg;
    }

    /**
     * 自增id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 对应的触发记录id
     */
    private Long triggerLogId;

    /**
     * 规则集合的id
     */
    private Long collectionId;

    /**
     * 对应的ruleInfoComputeId
     */
    private Long ruleInfoComputeId;

    /**
     * 报警消息
     */
    private String msg;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 是否发送成功
     */
    private Boolean ifSend;

    /**
     * 是否已读
     */
    private Boolean ifRead;
}
