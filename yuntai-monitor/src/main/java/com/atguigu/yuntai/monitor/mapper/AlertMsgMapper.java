package com.atguigu.yuntai.monitor.mapper;

import com.atguigu.yuntai.monitor.bean.AlertMsgRowVO;
import com.atguigu.yuntai.monitor.entity.AlertMsg;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 报警消息Mapper
 */
@DS("mysql-yuntai-monitor")
public interface AlertMsgMapper extends BaseMapper<AlertMsg> {

    @Select("select t1.*,t2.hive_database,t2.hive_table_name,t2.name collection_name,t3.name rule_compute_name,t4.who " +
            "from (select id,trigger_log_id,collection_id,rule_info_compute_id,msg,create_time,update_time,if_send,if_read" +
            "       from alert_msg " +
            "       ) t1 " +
            "left join rule_collection t2 " +
            "   on t1.collection_id = t2.id " +
            "left join rule_info_compute t3 " +
            "   on t1.rule_info_compute_id = t3.id " +
            "left join trigger_log t4 " +
            "   on t1.trigger_log_id = t4.id " +
            "order by t1.if_read asc,t1.id desc ")
    IPage<AlertMsgRowVO> listAlertMsgRowVO(Page<AlertMsgRowVO> page);


    @Select("select t1.*,t2.hive_database,t2.hive_table_name,t2.name collection_name,t3.name rule_compute_name,t4.who " +
            "from (select id,trigger_log_id,collection_id,rule_info_compute_id,msg,create_time,update_time,if_send,if_read" +
            "       from alert_msg " +
            "       where trigger_log_id = #{triggerLogId}" +
            "       ) t1 " +
            "left join rule_collection t2 " +
            "   on t1.collection_id = t2.id " +
            "left join rule_info_compute t3 " +
            "   on t1.rule_info_compute_id = t3.id " +
            "left join trigger_log t4 " +
            "   on t1.trigger_log_id = t4.id ")
    List<AlertMsgRowVO> listAlertMsgRowVo(@Param("triggerLogId") Long triggerId);
}
