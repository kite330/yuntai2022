package com.atguigu.yuntai.monitor.config;

import lombok.Data;
import org.apache.hadoop.hive.conf.HiveConf;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * <p>访问hive元数据的配置类</p>
 **/
@Configuration
@ConfigurationProperties(prefix = "hive.metastore")
@Data
public class HiveMetaStoreConfig {

    private String uris;

    @Bean
    @Qualifier("hiveMetaStoreConf")
    public HiveConf getConfiguration() {
        HiveConf hiveConf = new HiveConf();
        hiveConf.set("hive.metastore.uris",uris);
        return hiveConf;
    }


}
