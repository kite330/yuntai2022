package com.atguigu.yuntai.monitor.service;

import com.atguigu.yuntai.monitor.bean.PartitionDef;
import com.atguigu.yuntai.monitor.bean.PartitionResult;

/**
 *
 */
public interface ExecuteService {

    /**
     * 执行Dsl
     * @param dsl 分区表达式
     * @return 分区表达式的执行结果
     */
    String runDsl(String dsl);

    /**
     * 将分区定义对象，转变为分区执行结果对象。
     * @param partitionDef 分区定义对象
     * @return 分区执行结果的对象
     */
    PartitionResult partitionDef2Result(PartitionDef partitionDef);

    /**
     * 执行一个规则集合
     * @param triggerLogId 触发记录的id,传递这个参数是因为执行过程中要修改触发后执行的状态
     * @param collectionId 规则集合的id
     */
    void execute(Long triggerLogId, Long collectionId);
}
