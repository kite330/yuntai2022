package com.atguigu.yuntai.monitor.mapper;

import com.atguigu.yuntai.monitor.entity.RuleInfoCompute;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**

 */
@DS("mysql-yuntai-monitor")
public interface RuleInfoComputeMapper extends BaseMapper<RuleInfoCompute> {
}
