package com.atguigu.yuntai.report.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * hive操作mapper
 */
@DS("hive")
public interface HiveMapper {

    /**
     * 在hive中根据sql查询数据
     * @param sql
     * @return
     */
    @Select("${sql}")
    List<LinkedHashMap<String, Object>> findHiveList(@Param("sql") String sql);

    /**
     * 在hive中创建表
     * @param sql
     */
    @Insert("${sql}")
    void createTable(@Param("sql") String sql);
}
