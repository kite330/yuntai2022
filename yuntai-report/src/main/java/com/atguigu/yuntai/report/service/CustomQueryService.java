package com.atguigu.yuntai.report.service;

import com.atguigu.yuntai.report.bean.CustomQuery;
import com.atguigu.yuntai.report.bean.QCustomQuery;

import java.sql.SQLException;

/**
 * 多数据源操作服务接口
 */
public interface CustomQueryService {


    /**
     * 根据sql以及元数据组件查询结果，支持hive和presto查询
     *
     * @param qCustomQuery
     * @return
     * @throws SQLException
     */
    CustomQuery querySqlForType(QCustomQuery qCustomQuery) throws SQLException;

    void createTable(String sql);
}
