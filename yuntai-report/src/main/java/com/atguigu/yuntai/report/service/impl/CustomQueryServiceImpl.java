package com.atguigu.yuntai.report.service.impl;

import com.atguigu.yuntai.report.bean.CustomQuery;
import com.atguigu.yuntai.report.bean.QCustomQuery;
import com.atguigu.yuntai.report.bean.TableHeader;
import com.atguigu.yuntai.report.mapper.HiveMapper;
import com.atguigu.yuntai.report.mapper.PrestoMapper;
import com.atguigu.yuntai.report.service.CustomQueryService;
import com.mysql.jdbc.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.sql.SQLException;
import java.util.*;

/**
 * 多数据源操作实现类
 */
@Service
public class CustomQueryServiceImpl implements CustomQueryService {

    @Autowired
    private HiveMapper hiveMapper;

    @Autowired
    private PrestoMapper prestoMapper;

    /**
     * 根据sql以及元数据组件查询结果
     *
     * @param qCustomQuery
     * @return
     * @throws SQLException
     */
    @Override
    public CustomQuery querySqlForType(QCustomQuery qCustomQuery) throws SQLException {
        List<Map<String, Object >> query = new ArrayList<Map<String, Object >>();
        List<LinkedHashMap<String, Object>> maps = new ArrayList<LinkedHashMap<String, Object>>();
        if (qCustomQuery != null && !StringUtils.isNullOrEmpty(qCustomQuery.getTypeMetaData())) {
            //判断元数据类型，来确定使用哪种JdbcDriver
            if (qCustomQuery.getTypeMetaData().equals("hive")) {
                maps = hiveMapper.findHiveList(qCustomQuery.getSql());
            } else if (qCustomQuery.getTypeMetaData().equals("presto")) {
                maps = prestoMapper.findPrestoList(qCustomQuery.getSql());
            }
        }
        //将结果封装到QCustomQuery对象中
        CustomQuery returnQCustomQuery = new CustomQuery();
        for(LinkedHashMap linkedHashMap:maps){
            ArrayList<String> list = new ArrayList<>(linkedHashMap.keySet());
            if(!CollectionUtils.isEmpty(list)){
                for(String key:list){
                    Map<String,Object> map = (Map<String, Object>) linkedHashMap.get(key);
                    query.add(map);
                }
            }
        }
        returnQCustomQuery.setQueryMap(maps);
        returnQCustomQuery.setQuery(query);
        returnQCustomQuery.setTableHeaderList(tableHeader(maps));
        return returnQCustomQuery;
    }

    /**
     * 获取头信息
     *
     * @param maps
     * @return
     */
    public List<TableHeader> tableHeader(List<LinkedHashMap<String, Object>> maps) {
        List<TableHeader> tableHeaderList = new ArrayList<TableHeader>();
        if (!CollectionUtils.isEmpty(maps)) {
            Map<String, Object> result = maps.get(0);
            ArrayList<String> list = new ArrayList<>(result.keySet());
            String key = list.get(0);
            Map<String,Object> map = (Map<String, Object>) result.get(key);
            Set<String> keys = map.keySet();
            Collection<Object> values = map.values();
           /* Set<String> strings = result.keySet();

            HashMap<String,String> values = (HashMap<String, String>) result.values();
            Set<String> strings1 = values.keySet();
            if(!CollectionUtils.isEmpty(values)){
//               for(Object object : values){
////                   tableHeaderList.add(new TableHeader());
//               }

            }*/
//             values.entrySet();

            for (Map.Entry<String, Object> entry : map.entrySet()) {

                Object value = entry.getValue();
                tableHeaderList.add(new TableHeader(entry.getKey(),entry.getKey()));
            }
        }
        return tableHeaderList;
    }

    @Override
    public void createTable(String sql) {

        String s = sql.replaceAll("\n","\t");
//        hiveMapper.createTable(createTableBean.getSQL());
        hiveMapper.createTable(s);

    }

}
