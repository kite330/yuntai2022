package com.atguigu.yuntai.acl.mapper;

import com.atguigu.yuntai.acl.entity.RolePermission;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色权限Mpper接口
 */
@DS("mysql")
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
