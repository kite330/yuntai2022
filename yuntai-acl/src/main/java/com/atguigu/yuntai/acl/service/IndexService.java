package com.atguigu.yuntai.acl.service;

import java.util.Map;

/**
 * 后台登录与权限管理 服务接口
 */
public interface IndexService {

    /**
     * 根据用户名获取用户登录信息
     *
     * @param username
     * @return
     */
    Map<String, Object> getUserInfo(String username);

    /**
     * 根据用户名获取动态菜单
     *
     * @param username
     * @return
     */
    Map<String, Object> getMenu(String username);

}
