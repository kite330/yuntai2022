package com.atguigu.yuntai.acl.service;

import com.atguigu.yuntai.acl.bean.AdminQueryBean;
import com.atguigu.yuntai.acl.entity.Admin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户服务接口
 */
public interface AdminService extends IService<Admin> {

    /**
     * 用户分页列表
     *
     * @param pageParam
     * @param userQueryBean
     * @return
     */
    IPage<Admin> selectPage(Page<Admin> pageParam, AdminQueryBean userQueryBean);

    /**
     * 根据用户名查找用户
     *
     * @param username
     * @return
     */
    Admin selectByUsername(String username);
}
