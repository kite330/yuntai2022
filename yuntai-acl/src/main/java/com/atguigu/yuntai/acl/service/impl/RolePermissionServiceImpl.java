package com.atguigu.yuntai.acl.service.impl;

import com.atguigu.yuntai.acl.entity.RolePermission;
import com.atguigu.yuntai.acl.mapper.RolePermissionMapper;
import com.atguigu.yuntai.acl.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 角色权限服务实现类
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {


}
