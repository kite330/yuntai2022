package com.atguigu.yuntai.acl.mapper;

import com.atguigu.yuntai.acl.entity.AdminRole;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户角色Mpper接口
 */
@DS("mysql")
public interface AdminRoleMapper extends BaseMapper<AdminRole> {

}
