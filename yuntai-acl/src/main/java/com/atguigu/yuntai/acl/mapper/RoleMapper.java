package com.atguigu.yuntai.acl.mapper;

import com.atguigu.yuntai.acl.bean.RoleQueryBean;
import com.atguigu.yuntai.acl.entity.Role;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 角色Mpper接口
 */
@DS("mysql")
public interface RoleMapper extends BaseMapper<Role> {

    @Select("<script> " +
            "select id,role_name,remark,is_deleted,create_time,update_time " +
            "from role " +
            " <where> is_deleted = 0" +
            "  <if test=\"vo.roleName != null and vo.roleName != ''\">\n" +
            "  and role_name like CONCAT('%',#{vo.roleName},'%')\n" +
            "  </if>" +
            "  </where>" +
            "  order by id desc " +
            "</script> ")
    IPage<Role> selectPage(Page<Role> page, @Param("vo") RoleQueryBean roleQueryBean);
}
