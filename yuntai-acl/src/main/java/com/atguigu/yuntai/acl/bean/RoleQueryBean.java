//
//
package com.atguigu.yuntai.acl.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色查询实体
 */
@Data
@ApiModel(description = "角色查询实体")
public class RoleQueryBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

}

